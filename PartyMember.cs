﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PokeViewerLib
{
    #region Data Structs
    /// <summary>
    /// "Growth" data (held item, species, EXP, PP Ups, friendship)
    /// Don't ask why held item is in Growth.
    /// </summary>
    public class Growth
    {
        /// <summary>
        /// The index of the item being held by this Pokémon
        /// </summary>
        public ushort HeldItem { get; set; }

        /// <summary>
        /// The <seealso cref="Species"/> of this Pokémon
        /// </summary>
        public Species Species { get; set; }

        /// <summary>
        /// The total number of EXP. Points this Pokémon has
        /// </summary>
        public uint Experience { get; set; }

        /// <summary>
        /// Bitfield of PP Ups (2 bits per move, max. 3 PP ups per move)
        /// </summary>
        public byte PPBoosts { get; set; }

        /// <summary>
        /// Friendship value of this Pokémon
        /// </summary>
        public byte Friendship { get; set; }
    }

    /// <summary>
    /// The move data for this Pokémon
    /// </summary>
    public class MoveData
    {
        /// <summary>
        /// The array of <seealso cref="Move"/>s this Pokémon has learned
        /// </summary>
        public Move[] Moves { get; set; }

        /// <summary>
        /// The amount of PP each move has left
        /// </summary>
        public byte[] MovePP { get; set; }
    }

    /// <summary>
    /// Effor Value and Condition data
    /// </summary>
    public class EffortCondition
    {
        /// <summary>
        /// HP EVs
        /// </summary>
        public byte EffortHP { get; set; }

        /// <summary>
        /// Attack EVs
        /// </summary>
        public byte EffortAttack { get; set; }

        /// <summary>
        /// Defense EVs
        /// </summary>
        public byte EffortDefense { get; set; }

        /// <summary>
        /// Special Attack EVs
        /// </summary>
        public byte EffortSpAttack { get; set; }

        /// <summary>
        /// Special Defense EVs
        /// </summary>
        public byte EffortSpDefense { get; set; }

        /// <summary>
        /// Speed EVs
        /// </summary>
        public byte EffortSpeed { get; set; }

        /// <summary>
        /// Coolness condition
        /// </summary>
        public byte Coolness { get; set; }

        /// <summary>
        /// Beauty condition
        /// </summary>
        public byte Beauty { get; set; }

        /// <summary>
        /// Cuteness condition
        /// </summary>
        public byte Cuteness { get; set; }

        /// <summary>
        /// Smartness condition
        /// </summary>
        public byte Smartness { get; set; }

        /// <summary>
        /// Toughness condition
        /// </summary>
        public byte Toughness { get; set; }

        /// <summary>
        /// Pokémon feel (when full, can't eat any more PokéBlocks)
        /// </summary>
        public byte Feel { get; set; }
    }

    /// <summary>
    /// Miscellaneous data for this Pokémon
    /// </summary>
    public class MiscData
    {
        /// <summary>
        /// Pokérus status (infected, cured, days left, etc)
        /// </summary>
        public byte PokerusStatus { get; set; }

        /// <summary>
        /// Met-at location
        /// </summary>
        public byte LocationMetAt { get; set; }

        /// <summary>
        /// Origin information (game, route, level, etc)
        /// </summary>
        public ushort Origin { get; set; }

        /// <summary>
        /// Bitfield of Individual Values (each gets 5 bits if my memory serves me right)
        /// </summary>
        public uint IndividualValues { get; set; }

        /// <summary>
        /// Bitfield of Ribbons this Pokémon has earned
        /// </summary>
        public uint Ribbons { get; set; }
    }
    #endregion

    /// <summary>
    /// Represents the "data" field of a party member
    /// </summary>
    public class PartyMemberData
    {
        // These are the orders in which the four data structs are stored in the raw byte array
        // Each is 12 bytes long and the order is determined by (personality % 24)
        private static string[] DATA_BLOCK_ORDER = { "GAEM", "GAME", "GEAM", "GEMA", "GMAE", "GMEA", "AGEM", "AGME", "AEGM", "AEMG", "AMGE", "AMEG", "EGAM", "EGMA", "EAGM", "EAMG", "EMGA", "EMAG", "MGAE", "MGEA", "MAGE", "MAEG", "MEGA", "MEAG" };
        private const short DATA_BLOCK_SIZE = 12;

        /// <summary>
        /// The raw data loaded from memory
        /// </summary>
        private byte[] _rawData;

        /// <summary>
        /// <seealso cref="Growth"/> block
        /// </summary>
        public Growth Growth { get; private set; }

        /// <summary>
        /// <seealso cref="MoveData"/> block
        /// </summary>
        public MoveData Moves { get; private set; }

        /// <summary>
        /// <seealso cref="EffortCondition"/> block
        /// </summary>
        public EffortCondition EffortCondition { get; private set; }

        /// <summary>
        /// <seealso cref="MiscData"/> block
        /// </summary>
        public MiscData MiscData { get; private set; }

        /// <summary>
        /// Constructs, decrypts, and decodes a data field
        /// </summary>
        /// <param name="rawData">The raw data loaded from RAM</param>
        /// <param name="personality">The personality value of the Pokémon (needed for decrypting and decoding)</param>
        /// <param name="ownerID">The original trainer ID of this Pokémon (needed for decrypting)</param>
        public PartyMemberData(byte[] rawData, uint personality, uint ownerID)
        {
            this._rawData = rawData;
            DecryptRawData(ownerID, personality);
            LoadFromRawData(personality);
        }

        /// <summary>
        /// Decrypts the raw data (key is OwnerID XOR Personality, blocks of 4 bytes are then XORed with key to decrypt)
        /// </summary>
        /// <param name="ownerID">Original trainer ID of this Pokémon</param>
        /// <param name="personality">Personality value of this Pokémon</param>
        private void DecryptRawData(uint ownerID, uint personality)
        {
            uint key = ownerID ^ personality;
            for (int i = 0; i < this._rawData.Length; i += 4)
            {
                byte[] temp = this._rawData.Sub(i, 4);
                uint tChunk = ByteHelper.ByteArrayToInt(temp);
                tChunk ^= key;
                temp = ByteHelper.IntToByteArray(tChunk);
                this._rawData[i] = temp[0];
                this._rawData[i + 1] = temp[1];
                this._rawData[i + 2] = temp[2];
                this._rawData[i + 3] = temp[3];
            }
        }

        /// <summary>
        /// Loads the four blocks from the raw data in the order specified by (Personality MOD 24)
        /// </summary>
        /// <param name="personality">The personality value of this Pokémon</param>
        private void LoadFromRawData(uint personality)
        {
            string loadOrder = DATA_BLOCK_ORDER[personality % 24];
            short byteOffset = 0;
            byte[] tBuf;

            foreach (char c in loadOrder)
            {
                tBuf = this._rawData.Sub(byteOffset, DATA_BLOCK_SIZE);
                switch (c)
                {
                    case 'G': // Load Growth next
                        Growth g = new Growth();
                        ushort index = ByteHelper.ByteArrayToShort(tBuf.Sub(0, 2));
                        g.Species = Species.GetSpeciesByIndex(index);
                        g.HeldItem = ByteHelper.ByteArrayToShort(tBuf.Sub(2, 2));
                        g.Experience = ByteHelper.ByteArrayToInt(tBuf.Sub(4, 4));
                        g.PPBoosts = tBuf.Sub(8, 1)[0];
                        g.Friendship = tBuf.Sub(9, 1)[0];
                        this.Growth = g;
                        break;
                    case 'A': // Load Moves next
                        MoveData a = new MoveData();
                        a.Moves = new Move[4];
                        a.Moves[0] = Move.GetMoveByIndex(ByteHelper.ByteArrayToShort(tBuf.Sub(0, 2)));
                        a.Moves[1] = Move.GetMoveByIndex(ByteHelper.ByteArrayToShort(tBuf.Sub(2, 2)));
                        a.Moves[2] = Move.GetMoveByIndex(ByteHelper.ByteArrayToShort(tBuf.Sub(4, 2)));
                        a.Moves[3] = Move.GetMoveByIndex(ByteHelper.ByteArrayToShort(tBuf.Sub(6, 2)));
                        a.MovePP = tBuf.Sub(8, 4);
                        this.Moves = a;
                        break;
                    case 'E': // Load Effort & Condition next
                        EffortCondition e = new EffortCondition();
                        e.EffortHP = tBuf[0];
                        e.EffortAttack = tBuf[1];
                        e.EffortDefense = tBuf[2];
                        e.EffortSpeed = tBuf[3];
                        e.EffortSpAttack = tBuf[4];
                        e.EffortSpDefense = tBuf[5];
                        e.Coolness = tBuf[6];
                        e.Beauty = tBuf[7];
                        e.Cuteness = tBuf[8];
                        e.Smartness = tBuf[9];
                        e.Toughness = tBuf[10];
                        e.Feel = tBuf[11];
                        this.EffortCondition = e;
                        break;
                    case 'M': // Load Misc. Data next
                        MiscData m = new MiscData();
                        m.PokerusStatus = tBuf[0];
                        m.LocationMetAt = tBuf[1];
                        m.Origin = ByteHelper.ByteArrayToShort(tBuf.Sub(2, 2));
                        m.IndividualValues = ByteHelper.ByteArrayToInt(tBuf.Sub(4, 4));
                        m.Ribbons = ByteHelper.ByteArrayToInt(tBuf.Sub(8, 4));
                        this.MiscData = m;
                        break;
                }

                byteOffset += DATA_BLOCK_SIZE;
            }
        }
    }

    /// <summary>
    /// Represents one Pokémon in the party
    /// </summary>
    public class PartyMember
    {
        /// <summary>
        /// The personality value of this Pokémon
        /// </summary>
        public uint Personality { get; private set; }

        /// <summary>
        /// The original trainer ID of this Pokémon
        /// </summary>
        public uint OwnerID { get; private set; }

        /// <summary>
        /// This Pokémon's nickname
        /// </summary>
        public string Nickname { get; private set; }

        /// <summary>
        /// The game language this Pokémon originates from
        /// </summary>
        public ushort Language { get; private set; }

        /// <summary>
        /// The original trainer name of this Pokémon
        /// </summary>
        public string OwnerName { get; private set; }

        /// <summary>
        /// The markings this Pokémon has in the box (organizational)
        /// </summary>
        public byte Markings { get; private set; }

        /// <summary>
        /// The checksum of the <seealso cref="PartyMemberData"/> section
        /// </summary>
        public ushort Checksum { get; private set; }

        /// <summary>
        /// The <seealso cref="PartyMemberData"/> section
        /// </summary>
        public PartyMemberData Data { get; private set; }

        /// <summary>
        /// Bitfield of status ailments (sleep turns, paralyzed, poisoned, etc)
        /// </summary>
        public StatusCondition StatusCondition { get; private set; }

        /// <summary>
        /// The level of this Pokémon
        /// </summary>
        public byte Level { get; private set; }

        /// <summary>
        /// The amount of time before this Pokémon is cured of Pokérus
        /// </summary>
        public byte PokerusTime { get; private set; }

        /// <summary>
        /// This Pokémon's current health
        /// </summary>
        public ushort CurrentHP { get; private set; }

        /// <summary>
        /// The maximum amount of health this Pokémon can have
        /// </summary>
        public ushort MaximumHP { get; private set; }
        
        /// <summary>
        /// The attack stat of this Pokémon (calculated from IVs and EVs)
        /// </summary>
        public ushort Attack { get; private set; }

        /// <summary>
        /// The defense stat of this Pokémon (calculated from IVs and EVs)
        /// </summary>
        public ushort Defense { get; private set; }

        /// <summary>
        /// The speed stat of this Pokémon (calculated from IVs and EVs)
        /// </summary>
        public ushort Speed { get; private set; }

        /// <summary>
        /// The special attack stat of this Pokémon (calculated from IVs and EVs)
        /// </summary>
        public ushort SpecialAttack { get; private set; }

        /// <summary>
        /// The special defense stat of this Pokémon (calculated from IVs and EVs)
        /// </summary>
        public ushort SpecialDefense { get; private set; }

        /// <summary>
        /// Constructs an empty <seealso cref="PartyMember"/>
        /// </summary>
        public PartyMember()
        {
            Personality = OwnerID = 0;
            StatusCondition = new StatusCondition(0);
            Nickname = OwnerName = "";
            Markings = PokerusTime = 0;
            Level = 1;
            Language = Checksum = CurrentHP = MaximumHP = 0;
            Attack = Defense = Speed = SpecialAttack = SpecialDefense = 0;
            this.Data = new PartyMemberData(new byte[PokeGameHelper.SIZE_DATA], 0, 0);
        }

        /// <summary>
        /// Loads a party member from a block of memory
        /// </summary>
        /// <param name="partyData"></param>
        public void LoadFromDataArray(byte[] partyData)
        {
            this.Personality = ByteHelper.ByteArrayToInt(partyData.Sub(PokeGameHelper.OFF_PERSONALITY, PokeGameHelper.SIZE_PERSONALITY));
            this.OwnerID = ByteHelper.ByteArrayToInt(partyData.Sub(PokeGameHelper.OFF_OT_ID, PokeGameHelper.SIZE_OT_ID));
            this.Nickname = PokeGameHelper.PokeTextToString(partyData.Sub(PokeGameHelper.OFF_NICKNAME, PokeGameHelper.SIZE_NICKNAME));
            this.Language = ByteHelper.ByteArrayToShort(partyData.Sub(PokeGameHelper.OFF_LANGUAGE, PokeGameHelper.SIZE_LANGUAGE));
            this.OwnerName = PokeGameHelper.PokeTextToString(partyData.Sub(PokeGameHelper.OFF_OT_NAME, PokeGameHelper.SIZE_OT_NAME));
            this.Markings = partyData.Sub(PokeGameHelper.OFF_MARKINGS, PokeGameHelper.SIZE_MARKINGS)[0];
            this.Checksum = ByteHelper.ByteArrayToShort(partyData.Sub(PokeGameHelper.OFF_CHECKSUM, PokeGameHelper.SIZE_CHECKSUM));
            this.Data = new PartyMemberData(partyData.Sub(PokeGameHelper.OFF_DATA, PokeGameHelper.SIZE_DATA), this.Personality, this.OwnerID);
            this.StatusCondition = new StatusCondition(ByteHelper.ByteArrayToInt(partyData.Sub(PokeGameHelper.OFF_STATUS, PokeGameHelper.SIZE_STATUS)));
            this.Level = partyData.Sub(PokeGameHelper.OFF_LEVEL, PokeGameHelper.SIZE_LEVEL)[0];
            this.PokerusTime = partyData.Sub(PokeGameHelper.OFF_POKERUS, PokeGameHelper.SIZE_POKERUS)[0];
            this.CurrentHP = ByteHelper.ByteArrayToShort(partyData.Sub(PokeGameHelper.OFF_CUR_HP, PokeGameHelper.SIZE_CUR_HP));
            this.MaximumHP = ByteHelper.ByteArrayToShort(partyData.Sub(PokeGameHelper.OFF_MAX_HP, PokeGameHelper.SIZE_MAX_HP));
            this.Attack = ByteHelper.ByteArrayToShort(partyData.Sub(PokeGameHelper.OFF_ATTACK, PokeGameHelper.SIZE_ATTACK));
            this.Defense = ByteHelper.ByteArrayToShort(partyData.Sub(PokeGameHelper.OFF_DEFENSE, PokeGameHelper.SIZE_DEFENSE));
            this.Speed = ByteHelper.ByteArrayToShort(partyData.Sub(PokeGameHelper.OFF_SPEED, PokeGameHelper.SIZE_SPEED));
            this.SpecialAttack = ByteHelper.ByteArrayToShort(partyData.Sub(PokeGameHelper.OFF_SP_ATTACK, PokeGameHelper.SIZE_SP_ATTACK));
            this.SpecialDefense = ByteHelper.ByteArrayToShort(partyData.Sub(PokeGameHelper.OFF_SP_DEFENSE, PokeGameHelper.SIZE_SP_DEFENSE));
        }

        public bool IsShiny()
        {
            uint tID = this.OwnerID & 0xFFFFu;
            uint sID = this.OwnerID >> 16;
            uint lPID = this.Personality & 0xFFFFu;
            uint hPID = this.Personality >> 16;
            uint sV = (tID ^ sID) ^ (lPID ^ hPID);
            return sV < 8u;
        }

        /// <summary>
        /// Returns a string representation of this Pokémon
        /// </summary>
        /// <returns>A string containing common details about this Pokémon</returns>
        public override string ToString()
        {
            if (this.Data.Growth.Species == null)
                return "";
            return string.Format("#{0} {1} {2} (Lv. {3}): {4}/{5} HP ({6}%)", this.Data.Growth.Species.NationalDex.ToString().PadRight(3), this.Nickname.PadLeft(PokeGameHelper.SIZE_NICKNAME), string.Format("({0})", this.Data.Growth.Species.Name).PadRight(12), this.Level.ToString().PadLeft(3), this.CurrentHP, this.MaximumHP, ((this.MaximumHP == 0) ? 0 : (int)(((float)this.CurrentHP / (float)this.MaximumHP) * 100f)).ToString().PadLeft(3));
        }
    }
}
