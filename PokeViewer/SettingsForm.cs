﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PokeViewer
{
    public partial class SettingsForm : Form
    {
        private MainForm _mainForm;

        public SettingsForm(MainForm mainForm)
        {
            InitializeComponent();
            this._mainForm = mainForm;
        }

        public void UpdateSettings()
        {
            this.panelBackcolor.BackColor = this._mainForm.BackColor;
            this.panelForecolor.BackColor = this._mainForm.ForeColor;
            this.checkBoxFrame.Checked = this._mainForm.FormBorderStyle != System.Windows.Forms.FormBorderStyle.None;
            this.checkBoxCompact.Checked = this._mainForm.Compact;

            this.listBoxGame.Items.Clear();
            foreach (Tuple<string, int> t in PokeViewerLib.PokeGameHelper.POKEDATA_OFFSETS)
            {
                this.listBoxGame.Items.Add(t.Item1);
            }

            if (this._mainForm.Reader != null)
            {
                this.listBoxGame.Enabled = true;
                this.listBoxGame.SelectedIndex = this._mainForm.Reader.GameType;
            }
            else
            {
                this.listBoxGame.Enabled = false;
                this.listBoxGame.SelectedIndex = (this.listBoxGame.Items.Count > 0) ? 0 : -1;
            }
        }

        private void panelBackcolor_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            this.colorDialogBackColor.Color = this._mainForm.BackColor;
            if (this.colorDialogBackColor.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                this._mainForm.BackColor = this.colorDialogBackColor.Color;
                this.panelBackcolor.BackColor = this.colorDialogBackColor.Color;
                this._mainForm.SetColorInConfig("BackColor", this._mainForm.BackColor);
                this._mainForm.Config.SaveConfig();
            }
        }

        private void panelForecolor_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            this.colorDialogForeColor.Color = this._mainForm.ForeColor;
            if (this.colorDialogForeColor.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                this._mainForm.ForeColor = this.colorDialogForeColor.Color;
                this.panelForecolor.BackColor = this.colorDialogForeColor.Color;
                this._mainForm.SetColorInConfig("ForeColor", this._mainForm.ForeColor);
                this._mainForm.Config.SaveConfig();
            }
        }

        private void checkBoxFrame_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxFrame.Checked)
            {
                this._mainForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            }
            else
            {
                this._mainForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            }
            this._mainForm.Config.RootNode.SetValue("ShowBorder", this.checkBoxFrame.Checked);
            this._mainForm.Config.SaveConfig();
        }

        private void checkBoxCompact_CheckedChanged(object sender, EventArgs e)
        {
            this._mainForm.Compact = this.checkBoxCompact.Checked;
            this._mainForm.Config.RootNode.SetValue("Compact", this.checkBoxCompact.Checked);
            this._mainForm.Config.SaveConfig();
        }

        private void SettingsForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            this.Hide();
        }

        private void SettingsForm_Load(object sender, EventArgs e)
        {
            UpdateSettings();
        }

        private void listBoxGame_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this._mainForm.Reader != null)
            {
                this._mainForm.Reader.GameType = listBoxGame.SelectedIndex;
                this._mainForm.Config.RootNode.SetValue("Game", listBoxGame.SelectedIndex);
                this._mainForm.Config.SaveConfig();
            }
        }
    }
}
