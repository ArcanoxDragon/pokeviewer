﻿namespace PokeViewer
{
    partial class SettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SettingsForm));
            this.labelBackColor = new System.Windows.Forms.Label();
            this.colorDialogBackColor = new System.Windows.Forms.ColorDialog();
            this.panelBackcolor = new System.Windows.Forms.Panel();
            this.labelForecolor = new System.Windows.Forms.Label();
            this.panelForecolor = new System.Windows.Forms.Panel();
            this.colorDialogForeColor = new System.Windows.Forms.ColorDialog();
            this.checkBoxFrame = new System.Windows.Forms.CheckBox();
            this.checkBoxCompact = new System.Windows.Forms.CheckBox();
            this.toolTipOption = new System.Windows.Forms.ToolTip(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.listBoxGame = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // labelBackColor
            // 
            this.labelBackColor.AutoSize = true;
            this.labelBackColor.Location = new System.Drawing.Point(12, 15);
            this.labelBackColor.Name = "labelBackColor";
            this.labelBackColor.Size = new System.Drawing.Size(100, 13);
            this.labelBackColor.TabIndex = 0;
            this.labelBackColor.Text = "Window Backcolor:";
            // 
            // colorDialogBackColor
            // 
            this.colorDialogBackColor.AnyColor = true;
            this.colorDialogBackColor.FullOpen = true;
            // 
            // panelBackcolor
            // 
            this.panelBackcolor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelBackcolor.Location = new System.Drawing.Point(118, 12);
            this.panelBackcolor.Name = "panelBackcolor";
            this.panelBackcolor.Size = new System.Drawing.Size(24, 16);
            this.panelBackcolor.TabIndex = 1;
            this.panelBackcolor.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.panelBackcolor_MouseDoubleClick);
            // 
            // labelForecolor
            // 
            this.labelForecolor.AutoSize = true;
            this.labelForecolor.Location = new System.Drawing.Point(12, 37);
            this.labelForecolor.Name = "labelForecolor";
            this.labelForecolor.Size = new System.Drawing.Size(96, 13);
            this.labelForecolor.TabIndex = 0;
            this.labelForecolor.Text = "Window Forecolor:";
            // 
            // panelForecolor
            // 
            this.panelForecolor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelForecolor.Location = new System.Drawing.Point(118, 34);
            this.panelForecolor.Name = "panelForecolor";
            this.panelForecolor.Size = new System.Drawing.Size(24, 16);
            this.panelForecolor.TabIndex = 1;
            this.panelForecolor.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.panelForecolor_MouseDoubleClick);
            // 
            // colorDialogForeColor
            // 
            this.colorDialogForeColor.AnyColor = true;
            this.colorDialogForeColor.FullOpen = true;
            // 
            // checkBoxFrame
            // 
            this.checkBoxFrame.AutoSize = true;
            this.checkBoxFrame.Checked = true;
            this.checkBoxFrame.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxFrame.Location = new System.Drawing.Point(15, 56);
            this.checkBoxFrame.Name = "checkBoxFrame";
            this.checkBoxFrame.Size = new System.Drawing.Size(127, 17);
            this.checkBoxFrame.TabIndex = 2;
            this.checkBoxFrame.Text = "Show Window Frame";
            this.toolTipOption.SetToolTip(this.checkBoxFrame, "Window frame can also be\r\ntoggled by double-clicking\r\nthe main window.");
            this.checkBoxFrame.UseVisualStyleBackColor = true;
            this.checkBoxFrame.CheckedChanged += new System.EventHandler(this.checkBoxFrame_CheckedChanged);
            // 
            // checkBoxCompact
            // 
            this.checkBoxCompact.AutoSize = true;
            this.checkBoxCompact.Location = new System.Drawing.Point(15, 79);
            this.checkBoxCompact.Name = "checkBoxCompact";
            this.checkBoxCompact.Size = new System.Drawing.Size(97, 17);
            this.checkBoxCompact.TabIndex = 2;
            this.checkBoxCompact.Text = "Compact mode";
            this.toolTipOption.SetToolTip(this.checkBoxCompact, "Compact mode can also be\r\ntoggled by right-clicking the\r\nmain window.");
            this.checkBoxCompact.UseVisualStyleBackColor = true;
            this.checkBoxCompact.CheckedChanged += new System.EventHandler(this.checkBoxCompact_CheckedChanged);
            // 
            // toolTipOption
            // 
            this.toolTipOption.AutomaticDelay = 0;
            this.toolTipOption.AutoPopDelay = 5000;
            this.toolTipOption.InitialDelay = 0;
            this.toolTipOption.IsBalloon = true;
            this.toolTipOption.ReshowDelay = 0;
            this.toolTipOption.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.toolTipOption.ToolTipTitle = "Tip";
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles) ((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 99);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Game:";
            // 
            // listBoxGame
            // 
            this.listBoxGame.Anchor = ((System.Windows.Forms.AnchorStyles) (((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.listBoxGame.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.listBoxGame.FormattingEnabled = true;
            this.listBoxGame.Location = new System.Drawing.Point(12, 115);
            this.listBoxGame.Name = "listBoxGame";
            this.listBoxGame.Size = new System.Drawing.Size(229, 21);
            this.listBoxGame.TabIndex = 4;
            this.listBoxGame.SelectedIndexChanged += new System.EventHandler(this.listBoxGame_SelectedIndexChanged);
            // 
            // SettingsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(253, 148);
            this.Controls.Add(this.listBoxGame);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.checkBoxCompact);
            this.Controls.Add(this.checkBoxFrame);
            this.Controls.Add(this.panelForecolor);
            this.Controls.Add(this.labelForecolor);
            this.Controls.Add(this.panelBackcolor);
            this.Controls.Add(this.labelBackColor);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon) (resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "SettingsForm";
            this.Text = "PokéViewer Settings";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SettingsForm_FormClosing);
            this.Load += new System.EventHandler(this.SettingsForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelBackColor;
        private System.Windows.Forms.ColorDialog colorDialogBackColor;
        private System.Windows.Forms.Panel panelBackcolor;
        private System.Windows.Forms.Label labelForecolor;
        private System.Windows.Forms.Panel panelForecolor;
        private System.Windows.Forms.ColorDialog colorDialogForeColor;
        private System.Windows.Forms.CheckBox checkBoxFrame;
        private System.Windows.Forms.CheckBox checkBoxCompact;
        private System.Windows.Forms.ToolTip toolTipOption;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox listBoxGame;
    }
}