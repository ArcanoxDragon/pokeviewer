﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PokeViewer
{
    public partial class InputBox : Form
    {
        public InputBox(string prompt, string title)
        {
            InitializeComponent();
            this.Text = title;
            this.labelPrompt.Text = prompt;
        }

        public string InputText
        {
            get
            {
                return this.textBoxInput.Text;
            }
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }

        private void InputBox_Load(object sender, EventArgs e)
        {
            this.textBoxInput.Focus();
        }
    }
}
