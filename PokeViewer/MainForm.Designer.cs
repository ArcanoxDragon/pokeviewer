﻿namespace PokeViewer
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            PokeViewerLib.PartyMember partyMember1 = new PokeViewerLib.PartyMember();
            PokeViewerLib.PartyMember partyMember2 = new PokeViewerLib.PartyMember();
            PokeViewerLib.PartyMember partyMember3 = new PokeViewerLib.PartyMember();
            PokeViewerLib.PartyMember partyMember4 = new PokeViewerLib.PartyMember();
            PokeViewerLib.PartyMember partyMember5 = new PokeViewerLib.PartyMember();
            PokeViewerLib.PartyMember partyMember6 = new PokeViewerLib.PartyMember();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.timerUpdate = new System.Windows.Forms.Timer(this.components);
            this.buttonSettings = new System.Windows.Forms.Button();
            this.buttonSetProcess = new System.Windows.Forms.Button();
            this.partyView5 = new PokeViewer.PartyView();
            this.partyView3 = new PokeViewer.PartyView();
            this.partyView1 = new PokeViewer.PartyView();
            this.partyView4 = new PokeViewer.PartyView();
            this.partyView2 = new PokeViewer.PartyView();
            this.partyView0 = new PokeViewer.PartyView();
            this.SuspendLayout();
            // 
            // timerUpdate
            // 
            this.timerUpdate.Enabled = true;
            this.timerUpdate.Interval = 150;
            this.timerUpdate.Tick += new System.EventHandler(this.timerUpdate_Tick);
            // 
            // buttonSettings
            // 
            this.buttonSettings.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonSettings.ForeColor = System.Drawing.SystemColors.WindowText;
            this.buttonSettings.Location = new System.Drawing.Point(238, 12);
            this.buttonSettings.Name = "buttonSettings";
            this.buttonSettings.Size = new System.Drawing.Size(116, 23);
            this.buttonSettings.TabIndex = 3;
            this.buttonSettings.TabStop = false;
            this.buttonSettings.Text = "Show Settings";
            this.buttonSettings.UseVisualStyleBackColor = true;
            this.buttonSettings.Click += new System.EventHandler(this.buttonSettings_Click);
            // 
            // buttonSetProcess
            // 
            this.buttonSetProcess.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonSetProcess.ForeColor = System.Drawing.SystemColors.WindowText;
            this.buttonSetProcess.Location = new System.Drawing.Point(238, 41);
            this.buttonSetProcess.Name = "buttonSetProcess";
            this.buttonSetProcess.Size = new System.Drawing.Size(116, 23);
            this.buttonSetProcess.TabIndex = 2;
            this.buttonSetProcess.TabStop = false;
            this.buttonSetProcess.Text = "Set Emulator Process";
            this.buttonSetProcess.UseVisualStyleBackColor = true;
            this.buttonSetProcess.Click += new System.EventHandler(this.buttonConnect_Click);
            // 
            // partyView5
            // 
            this.partyView5.BackColor = System.Drawing.Color.Transparent;
            this.partyView5.Compact = false;
            this.partyView5.Font = new System.Drawing.Font("Pokemon Solid", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.partyView5.Location = new System.Drawing.Point(0, 361);
            this.partyView5.Margin = new System.Windows.Forms.Padding(0);
            this.partyView5.MaximumSize = new System.Drawing.Size(230, 72);
            this.partyView5.MinimumSize = new System.Drawing.Size(230, 32);
            this.partyView5.Name = "partyView5";
            this.partyView5.PartyMember = partyMember1;
            this.partyView5.Size = new System.Drawing.Size(230, 72);
            this.partyView5.TabIndex = 14;
            this.partyView5.TabStop = false;
            this.partyView5.DoubleClick += new System.EventHandler(this.OnDoubleClick);
            this.partyView5.MouseDown += new System.Windows.Forms.MouseEventHandler(this.OnMouseDown);
            // 
            // partyView3
            // 
            this.partyView3.BackColor = System.Drawing.Color.Transparent;
            this.partyView3.Compact = false;
            this.partyView3.Font = new System.Drawing.Font("Pokemon Solid", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.partyView3.Location = new System.Drawing.Point(0, 216);
            this.partyView3.Margin = new System.Windows.Forms.Padding(0);
            this.partyView3.MaximumSize = new System.Drawing.Size(230, 72);
            this.partyView3.MinimumSize = new System.Drawing.Size(230, 32);
            this.partyView3.Name = "partyView3";
            this.partyView3.PartyMember = partyMember2;
            this.partyView3.Size = new System.Drawing.Size(230, 72);
            this.partyView3.TabIndex = 15;
            this.partyView3.TabStop = false;
            this.partyView3.DoubleClick += new System.EventHandler(this.OnDoubleClick);
            this.partyView3.MouseDown += new System.Windows.Forms.MouseEventHandler(this.OnMouseDown);
            // 
            // partyView1
            // 
            this.partyView1.BackColor = System.Drawing.Color.Transparent;
            this.partyView1.Compact = false;
            this.partyView1.Font = new System.Drawing.Font("Pokemon Solid", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.partyView1.Location = new System.Drawing.Point(0, 72);
            this.partyView1.Margin = new System.Windows.Forms.Padding(0);
            this.partyView1.MaximumSize = new System.Drawing.Size(230, 72);
            this.partyView1.MinimumSize = new System.Drawing.Size(230, 32);
            this.partyView1.Name = "partyView1";
            this.partyView1.PartyMember = partyMember3;
            this.partyView1.Size = new System.Drawing.Size(230, 72);
            this.partyView1.TabIndex = 16;
            this.partyView1.TabStop = false;
            this.partyView1.DoubleClick += new System.EventHandler(this.OnDoubleClick);
            this.partyView1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.OnMouseDown);
            // 
            // partyView4
            // 
            this.partyView4.BackColor = System.Drawing.Color.Transparent;
            this.partyView4.Compact = false;
            this.partyView4.Font = new System.Drawing.Font("Pokemon Solid", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.partyView4.Location = new System.Drawing.Point(0, 288);
            this.partyView4.Margin = new System.Windows.Forms.Padding(0);
            this.partyView4.MaximumSize = new System.Drawing.Size(230, 72);
            this.partyView4.MinimumSize = new System.Drawing.Size(230, 32);
            this.partyView4.Name = "partyView4";
            this.partyView4.PartyMember = partyMember4;
            this.partyView4.Size = new System.Drawing.Size(230, 72);
            this.partyView4.TabIndex = 11;
            this.partyView4.TabStop = false;
            this.partyView4.DoubleClick += new System.EventHandler(this.OnDoubleClick);
            this.partyView4.MouseDown += new System.Windows.Forms.MouseEventHandler(this.OnMouseDown);
            // 
            // partyView2
            // 
            this.partyView2.BackColor = System.Drawing.Color.Transparent;
            this.partyView2.Compact = false;
            this.partyView2.Font = new System.Drawing.Font("Pokemon Solid", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.partyView2.Location = new System.Drawing.Point(0, 144);
            this.partyView2.Margin = new System.Windows.Forms.Padding(0);
            this.partyView2.MaximumSize = new System.Drawing.Size(230, 72);
            this.partyView2.MinimumSize = new System.Drawing.Size(230, 32);
            this.partyView2.Name = "partyView2";
            this.partyView2.PartyMember = partyMember5;
            this.partyView2.Size = new System.Drawing.Size(230, 72);
            this.partyView2.TabIndex = 12;
            this.partyView2.TabStop = false;
            this.partyView2.DoubleClick += new System.EventHandler(this.OnDoubleClick);
            this.partyView2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.OnMouseDown);
            // 
            // partyView0
            // 
            this.partyView0.BackColor = System.Drawing.Color.Transparent;
            this.partyView0.Compact = false;
            this.partyView0.Font = new System.Drawing.Font("Pokemon Solid", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.partyView0.Location = new System.Drawing.Point(0, 0);
            this.partyView0.Margin = new System.Windows.Forms.Padding(0);
            this.partyView0.MaximumSize = new System.Drawing.Size(230, 72);
            this.partyView0.MinimumSize = new System.Drawing.Size(230, 32);
            this.partyView0.Name = "partyView0";
            this.partyView0.PartyMember = partyMember6;
            this.partyView0.Size = new System.Drawing.Size(230, 72);
            this.partyView0.TabIndex = 13;
            this.partyView0.TabStop = false;
            this.partyView0.DoubleClick += new System.EventHandler(this.OnDoubleClick);
            this.partyView0.MouseDown += new System.Windows.Forms.MouseEventHandler(this.OnMouseDown);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(366, 432);
            this.Controls.Add(this.partyView5);
            this.Controls.Add(this.partyView3);
            this.Controls.Add(this.partyView1);
            this.Controls.Add(this.partyView4);
            this.Controls.Add(this.partyView2);
            this.Controls.Add(this.partyView0);
            this.Controls.Add(this.buttonSettings);
            this.Controls.Add(this.buttonSetProcess);
            this.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "PokéViewer";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainForm_FormClosed);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.DoubleClick += new System.EventHandler(this.OnDoubleClick);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.OnMouseDown);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Timer timerUpdate;
        private System.Windows.Forms.Button buttonSettings;
        private System.Windows.Forms.Button buttonSetProcess;
        private PartyView partyView5;
        private PartyView partyView3;
        private PartyView partyView1;
        private PartyView partyView4;
        private PartyView partyView2;
        private PartyView partyView0;
    }
}

