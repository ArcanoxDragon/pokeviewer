﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using PokeViewerLib;
using System.IO;
using PokeViewer.Config;
using System.Net;

namespace PokeViewer
{
    public partial class MainForm : Form
    {
        private const int MAXIMIZED_WIDTH = 366;

        private SettingsForm _settingsForm;
        private PokeDataReader _reader;
        private bool _compact;

        public ConfigFile Config { get; private set; }
        public PokeDataReader Reader
        {
            get
            {
                return this._reader;
            }
        }

        public bool Compact
        {
            get
            {
                return this._compact;
            }
            set
            {
                this._compact = value;
                this.partyView0.Compact = value;
                this.partyView1.Compact = value;
                this.partyView2.Compact = value;
                this.partyView3.Compact = value;
                this.partyView4.Compact = value;
                this.partyView5.Compact = value;
                this.buttonSetProcess.Visible = !value;
                this.buttonSettings.Visible = !value;
                this.ClientSize = new Size(this._compact ? partyView0.ClientSize.Width : MAXIMIZED_WIDTH, partyView0.ClientSize.Height * 6);
                RealignParty();
            }
        }

        public MainForm()
        {
            InitializeComponent();
            PokeViewerLib.Move.LoadMoves(new FileStream("moves.db", FileMode.Open));
            PokeViewerLib.Species.LoadSpeciesList(new FileStream("species.db", FileMode.Open));
            this.Config = new ConfigFile(".", "PokeViewer", BuildDefaultConfig);
            bool compact;
            if (!Config.GetValue<bool>("Compact", out compact))
                compact = false;
            this.Compact = compact;
            string procName;
            Config.GetValue<string>("ProcessName", out procName);
            ProcessWrapper pw = ProcessWrapper.GetWrapperByProcessName(procName);
            try
            {
                this._reader = (pw == null) ? null : new PokeDataReader(pw);
            }
            catch (Exception ex)
            {
                Console.WriteLine("#### ERROR ####\n{0}\n\n", ex.ToString());
                MessageBox.Show("Error: " + ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            this.BackColor = GetColorFromConfig("BackColor");
            this.ForeColor = GetColorFromConfig("ForeColor");
            bool border;
            if (Config.GetValue<bool>("ShowBorder", out border))
                this.FormBorderStyle = border ? FormBorderStyle.FixedSingle : FormBorderStyle.None;
            else
                this.FormBorderStyle = FormBorderStyle.FixedSingle;
            if (this._reader != null)
            {
                int gameType;
                if (!Config.GetValue<int>("Game", out gameType))
                    gameType = 0;
                this._reader.GameType = gameType;
            }
            this._settingsForm = new SettingsForm(this);
        }

        private void RealignParty()
        {
            partyView0.Left = partyView1.Left = partyView2.Left = partyView3.Left = partyView4.Left = partyView5.Left = 0;
            partyView0.Top = 0;
            partyView1.Top = partyView0.ClientSize.Height;
            partyView2.Top = partyView0.ClientSize.Height * 2;
            partyView3.Top = partyView0.ClientSize.Height * 3;
            partyView4.Top = partyView0.ClientSize.Height * 4;
            partyView5.Top = partyView0.ClientSize.Height * 5;
        }

        public Color GetColorFromConfig(string keyName)
        {
            int R, G, B;
            if (Config.GetValue<int>(keyName + "R", out R) && Config.GetValue<int>(keyName + "G", out G) && Config.GetValue<int>(keyName + "B", out B))
            {
                return Color.FromArgb(255, R, G, B);
            }
            return Color.Black;
        }

        public void SetColorInConfig(string keyName, Color color)
        {
            Config.RootNode.SetValue(keyName + "R", color.R);
            Config.RootNode.SetValue(keyName + "G", color.G);
            Config.RootNode.SetValue(keyName + "B", color.B);
        }

        private void BuildDefaultConfig(ConfigFile config)
        {
            config.RootNode.SetValue("BackColorR", 0);
            config.RootNode.SetValue("BackColorG", 0);
            config.RootNode.SetValue("BackColorB", 0);
            config.RootNode.SetValue("ForeColorR", 255);
            config.RootNode.SetValue("ForeColorG", 255);
            config.RootNode.SetValue("ForeColorB", 255);
            config.RootNode.SetValue("ShowBorder", true);
            config.RootNode.SetValue("Compact", false);
            config.RootNode.SetValue("ProcessName", "VisualBoyAdvance");
            config.RootNode.SetValue("Game", 1); // Emerald (US)
        }

        private void buttonConnect_Click(object sender, EventArgs e)
        {
            this._reader = null;
            InputBox ip = new InputBox("Enter the name of the emulator process (without \".exe\"):", "Connect to Emulator");
            if (ip.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                ProcessWrapper pw = ProcessWrapper.GetWrapperByProcessName(ip.InputText);
                if (pw == null)
                {
                    MessageBox.Show(string.Format("Could not find a process with the name \"{0}\".", ip.InputText), "Process Not Found", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    try
                    {
                        this._reader = new PokeDataReader(pw);
                        if (this._reader != null)
                        {
                            int gameType;
                            if (!Config.GetValue<int>("Game", out gameType))
                                gameType = 0;
                            this._reader.GameType = gameType;
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("#### ERROR ####\n{0}\n\n", ex.ToString());
                        MessageBox.Show("Error: " + ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    Config.RootNode.SetValue("ProcessName", ip.InputText);
                    Config.SaveConfig();
                    this._settingsForm.UpdateSettings();
                }
            }
        }

        private void timerUpdate_Tick(object sender, EventArgs e)
        {
            if (Cursor.Position.X >= this.Location.X && Cursor.Position.X <= this.Location.X + this.Width &&
                Cursor.Position.Y >= this.Location.Y && Cursor.Position.Y <= this.Location.Y + this.Height)
            {
                if (!this._compact)
                {
                    this.buttonSettings.Visible = true;
                    this.buttonSetProcess.Visible = true;
                }
                else
                {
                    this.buttonSettings.Visible = true;
                    this.buttonSetProcess.Visible = true;
                }
            }
            else
            {
                this.buttonSettings.Visible = false;
                this.buttonSetProcess.Visible = false;
            }

            buttonSettings.Text = string.Format("{0} Settings", this._settingsForm.Visible ? "Hide" : "Show");

            if (this._reader != null)
            {
                PartyMember party0 = this.partyView0.PartyMember;
                PartyMember party1 = this.partyView1.PartyMember;
                PartyMember party2 = this.partyView2.PartyMember;
                PartyMember party3 = this.partyView3.PartyMember;
                PartyMember party4 = this.partyView4.PartyMember;
                PartyMember party5 = this.partyView5.PartyMember;
                this._reader.UpdatePartyMember(ref party0, 0);
                this._reader.UpdatePartyMember(ref party1, 1);
                this._reader.UpdatePartyMember(ref party2, 2);
                this._reader.UpdatePartyMember(ref party3, 3);
                this._reader.UpdatePartyMember(ref party4, 4);
                this._reader.UpdatePartyMember(ref party5, 5);
            }
            else
            {
                this.partyView0.PartyMember.Data.Growth.Species = null;
                this.partyView1.PartyMember.Data.Growth.Species = null;
                this.partyView2.PartyMember.Data.Growth.Species = null;
                this.partyView3.PartyMember.Data.Growth.Species = null;
                this.partyView4.PartyMember.Data.Growth.Species = null;
                this.partyView5.PartyMember.Data.Growth.Species = null;
            }

            this.partyView0.Refresh();
            this.partyView1.Refresh();
            this.partyView2.Refresh();
            this.partyView3.Refresh();
            this.partyView4.Refresh();
            this.partyView5.Refresh();
        }

        private void buttonSettings_Click(object sender, EventArgs e)
        {
            if (!this._settingsForm.Visible)
                this._settingsForm.Show();
            else
                this._settingsForm.Hide();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            bool haveAllSprites = true;
            for (int i = 1; i <= SpriteDownloader.TOTAL_SPRITES; i++)
            {
                if (!File.Exists(string.Format("Images/Sprite_{0}.png", i.ToString("000"))))
                    haveAllSprites = false;
                if (!File.Exists(string.Format("Images/Sprite_{0}s.png", i.ToString("000"))))
                    haveAllSprites = false;
                if (!File.Exists(string.Format("Images/Icon_{0}.png", i.ToString("000"))))
                    haveAllSprites = false;
            }
            if (!haveAllSprites)
            {
                try
                {
                    if (new SpriteDownloader().ShowDialog() == DialogResult.Cancel)
                    {
                        Application.Exit();
                    }
                }
                catch
                {
                    Application.Exit();
                }
            }
        }

        private void OnMouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                this.Compact = !this.Compact;
                this._settingsForm.UpdateSettings();
                this.Config.RootNode.SetValue("Compact", this.Compact);
                this.Config.SaveConfig();
            }
        }

        private void OnDoubleClick(object sender, EventArgs e)
        {
            if (this.FormBorderStyle == FormBorderStyle.None)
            {
                this.FormBorderStyle = FormBorderStyle.FixedSingle;
            }
            else
            {
                this.FormBorderStyle = FormBorderStyle.None;
            }
            this._settingsForm.UpdateSettings();
            this.Config.RootNode.SetValue("ShowBorder", this.FormBorderStyle != FormBorderStyle.None);
            this.Config.SaveConfig();
        }

        private void MainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }
    }
}
