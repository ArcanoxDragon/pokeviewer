﻿namespace PokeViewer
{
    partial class PartyView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.spriteBox = new System.Windows.Forms.PictureBox();
            this.panelStats = new System.Windows.Forms.Panel();
            this.labelLevel = new System.Windows.Forms.Label();
            this.labelStatus = new System.Windows.Forms.Label();
            this.panelHP = new System.Windows.Forms.Panel();
            this.labelNickname = new System.Windows.Forms.Label();
            this.labelHP = new System.Windows.Forms.Label();
            this.labelSpecies = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.spriteBox)).BeginInit();
            this.panelStats.SuspendLayout();
            this.SuspendLayout();
            // 
            // spriteBox
            // 
            this.spriteBox.BackgroundImage = global::PokeViewer.Properties.Resources.pokeball_large;
            this.spriteBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.spriteBox.Dock = System.Windows.Forms.DockStyle.Left;
            this.spriteBox.Location = new System.Drawing.Point(0, 0);
            this.spriteBox.Name = "spriteBox";
            this.spriteBox.Size = new System.Drawing.Size(72, 72);
            this.spriteBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.spriteBox.TabIndex = 0;
            this.spriteBox.TabStop = false;
            this.spriteBox.DoubleClick += new System.EventHandler(this.ControlDoubleClick);
            this.spriteBox.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ControlMouseDown);
            // 
            // panelStats
            // 
            this.panelStats.Controls.Add(this.panelHP);
            this.panelStats.Controls.Add(this.labelLevel);
            this.panelStats.Controls.Add(this.labelStatus);
            this.panelStats.Controls.Add(this.labelNickname);
            this.panelStats.Controls.Add(this.labelHP);
            this.panelStats.Controls.Add(this.labelSpecies);
            this.panelStats.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelStats.Location = new System.Drawing.Point(78, 0);
            this.panelStats.Name = "panelStats";
            this.panelStats.Size = new System.Drawing.Size(152, 72);
            this.panelStats.TabIndex = 5;
            this.panelStats.DoubleClick += new System.EventHandler(this.ControlDoubleClick);
            this.panelStats.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ControlMouseDown);
            // 
            // labelLevel
            // 
            this.labelLevel.AutoSize = true;
            this.labelLevel.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelLevel.Location = new System.Drawing.Point(3, 18);
            this.labelLevel.Name = "labelLevel";
            this.labelLevel.Size = new System.Drawing.Size(29, 13);
            this.labelLevel.TabIndex = 6;
            this.labelLevel.Text = "Lv. 0";
            this.labelLevel.Visible = false;
            this.labelLevel.DoubleClick += new System.EventHandler(this.ControlDoubleClick);
            this.labelLevel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ControlMouseDown);
            // 
            // labelStatus
            // 
            this.labelStatus.BackColor = System.Drawing.Color.Firebrick;
            this.labelStatus.Font = new System.Drawing.Font("Pokemon GB", 6F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelStatus.ForeColor = System.Drawing.Color.White;
            this.labelStatus.Location = new System.Drawing.Point(6, 46);
            this.labelStatus.Name = "labelStatus";
            this.labelStatus.Size = new System.Drawing.Size(32, 14);
            this.labelStatus.TabIndex = 10;
            this.labelStatus.Text = "FNT";
            this.labelStatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panelHP
            // 
            this.panelHP.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelHP.Location = new System.Drawing.Point(6, 33);
            this.panelHP.Name = "panelHP";
            this.panelHP.Size = new System.Drawing.Size(142, 10);
            this.panelHP.TabIndex = 8;
            this.panelHP.Paint += new System.Windows.Forms.PaintEventHandler(this.panelHP_Paint);
            this.panelHP.DoubleClick += new System.EventHandler(this.ControlDoubleClick);
            this.panelHP.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ControlMouseDown);
            // 
            // labelNickname
            // 
            this.labelNickname.AutoSize = true;
            this.labelNickname.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNickname.Location = new System.Drawing.Point(3, 1);
            this.labelNickname.Name = "labelNickname";
            this.labelNickname.Size = new System.Drawing.Size(42, 15);
            this.labelNickname.TabIndex = 5;
            this.labelNickname.Text = "Empty";
            this.labelNickname.DoubleClick += new System.EventHandler(this.ControlDoubleClick);
            this.labelNickname.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ControlMouseDown);
            // 
            // labelHP
            // 
            this.labelHP.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelHP.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelHP.Location = new System.Drawing.Point(38, 18);
            this.labelHP.Name = "labelHP";
            this.labelHP.Size = new System.Drawing.Size(110, 13);
            this.labelHP.TabIndex = 9;
            this.labelHP.Text = "0/0";
            this.labelHP.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.labelHP.DoubleClick += new System.EventHandler(this.ControlDoubleClick);
            this.labelHP.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ControlMouseDown);
            // 
            // labelSpecies
            // 
            this.labelSpecies.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelSpecies.BackColor = System.Drawing.Color.Transparent;
            this.labelSpecies.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSpecies.Location = new System.Drawing.Point(56, 2);
            this.labelSpecies.Name = "labelSpecies";
            this.labelSpecies.Size = new System.Drawing.Size(93, 13);
            this.labelSpecies.TabIndex = 7;
            this.labelSpecies.Text = "(NULL)";
            this.labelSpecies.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.labelSpecies.DoubleClick += new System.EventHandler(this.ControlDoubleClick);
            this.labelSpecies.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ControlMouseDown);
            // 
            // PartyView
            // 
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.panelStats);
            this.Controls.Add(this.spriteBox);
            this.DoubleBuffered = true;
            this.Margin = new System.Windows.Forms.Padding(0);
            this.MaximumSize = new System.Drawing.Size(230, 72);
            this.MinimumSize = new System.Drawing.Size(230, 32);
            this.Name = "PartyView";
            this.Size = new System.Drawing.Size(230, 72);
            ((System.ComponentModel.ISupportInitialize)(this.spriteBox)).EndInit();
            this.panelStats.ResumeLayout(false);
            this.panelStats.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox spriteBox;
        private System.Windows.Forms.Panel panelStats;
        private System.Windows.Forms.Label labelNickname;
        private System.Windows.Forms.Label labelHP;
        private System.Windows.Forms.Panel panelHP;
        private System.Windows.Forms.Label labelLevel;
        private System.Windows.Forms.Label labelSpecies;
        private System.Windows.Forms.Label labelStatus;
    }
}
