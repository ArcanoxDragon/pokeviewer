﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.IO;
using System.Threading;

namespace PokeViewer
{
    public partial class SpriteDownloader : Form
    {
        public const int TOTAL_SPRITES = 386;

        private bool downloading;
        private bool allDownloaded = false;
        private int pFileTotal = 0;
        private int pFileCur = 0;
        private int pNumFiles = 0;
        private int pCurFile = 0;
        private string curFileName = "";
        private string status = "";
        private bool cancelDownload = false;

        public SpriteDownloader()
        {
            InitializeComponent();
        }

        void DownloadFileProgress(object sender, DownloadProgressChangedEventArgs e)
        {
            pFileTotal = (int) (e.TotalBytesToReceive);
            pFileCur = (int) (e.BytesReceived);
        }

        void DownloadFileCompleted(object sender, AsyncCompletedEventArgs e)
        {
            downloading = false;
            if (e.Error != null && !e.Cancelled)
            {
                if (File.Exists(curFileName))
                    File.Delete(curFileName);
                cancelDownload = true;
                Console.WriteLine("An error occurred while downloading {0}:\n\n{1}\n\nThe program cannot continue.", curFileName, e.Error.ToString());
                MessageBox.Show(string.Format("An error occurred while downloading {0}:\n\n{1}\n\nThe program cannot continue.", curFileName, e.Error.ToString()), "Download Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void SpriteDownloader_Load(object sender, EventArgs e)
        {
            Thread downloadThread = new Thread(new ThreadStart(DownloadThread));
            downloadThread.Name = "PokeViewer Image Downloader";
            downloadThread.Start();
        }

        private void DownloadThread()
        {
            allDownloaded = false;
            WebClient client = new WebClient();
            client.DownloadProgressChanged += DownloadFileProgress;
            client.DownloadFileCompleted += DownloadFileCompleted;

            if (!Directory.Exists("Images"))
                Directory.CreateDirectory("Images");

            pNumFiles = TOTAL_SPRITES * 3;

            // Icons
            for (int i = 1; i <= TOTAL_SPRITES && !cancelDownload; i++)
            {
                pCurFile = i;
                if (!File.Exists(string.Format("Images/Icon_{0}.png", i.ToString("000"))))
                {
                    Console.WriteLine("Downloading icon for #{0}...", i.ToString("000"));
                    status = string.Format("Preparing to download icon for #{0}...", i.ToString("000"));
                    downloading = true;
                    curFileName = string.Format("Images/Icon_{0}.png", i.ToString("000"));
                    status = string.Format("Downloading icon for #{0}...", i.ToString("000"));
                    client.DownloadFileAsync(new UriBuilder(string.Format("http://www.serebii.net/pokedex-xy/icon/{0}.png", i.ToString("000"))).Uri, string.Format("Images/Icon_{0}.png", i.ToString("000")));
                    while (downloading && !cancelDownload)
                        Thread.Sleep(50);
                    status = string.Format("Downloaded icon for #{0}.", i.ToString("000"));
                }
            }

            // Sprites
            for (int i = 1; i <= TOTAL_SPRITES && !cancelDownload; i++)
            {
                pCurFile = TOTAL_SPRITES + i;
                if (!File.Exists(string.Format("Images/Sprite_{0}.png", i.ToString("000"))))
                {
                    Console.WriteLine("Downloading sprite for #{0}...", i.ToString("000"));
                    status = string.Format("Preparing to download sprite for #{0}...", i.ToString("000"));
                    downloading = true;
                    curFileName = string.Format("Images/Sprite_{0}.png", i.ToString("000"));
                    status = string.Format("Downloading sprite for #{0}...", i.ToString("000"));
                    client.DownloadFileAsync(new UriBuilder(string.Format("http://www.serebii.net/emerald/pokemon/{0}.png", i.ToString("000"))).Uri, string.Format("Images/Sprite_{0}.png", i.ToString("000")));
                    while (downloading && !cancelDownload)
                        Thread.Sleep(50);
                    status = string.Format("Downloaded sprite for #{0}.", i.ToString("000"));
                }
            }

            // Sprites
            for (int i = 1; i <= TOTAL_SPRITES && !cancelDownload; i++)
            {
                pCurFile = TOTAL_SPRITES + i;
                if (!File.Exists(string.Format("Images/Sprite_{0}s.png", i.ToString("000"))))
                {
                    Console.WriteLine("Downloading shiny sprite for #{0}...", i.ToString("000"));
                    status = string.Format("Preparing to download shiny sprite for #{0}...", i.ToString("000"));
                    downloading = true;
                    curFileName = string.Format("Images/Sprite_{0}s.png", i.ToString("000"));
                    status = string.Format("Downloading shiny sprite for #{0}...", i.ToString("000"));
                    if (i == 386) // Why can't they be consistent...
                        client.DownloadFileAsync(new UriBuilder(string.Format("http://www.serebii.net/emerald/pokemon/{0}s.png", i.ToString("000"))).Uri, string.Format("Images/Sprite_{0}s.png", i.ToString("000")));
                    else
                        client.DownloadFileAsync(new UriBuilder(string.Format("http://www.serebii.net/Shiny/Em/{0}.png", i.ToString("000"))).Uri, string.Format("Images/Sprite_{0}s.png", i.ToString("000")));
                    while (downloading && !cancelDownload)
                        Thread.Sleep(50);
                    status = string.Format("Downloaded shiny sprite for #{0}.", i.ToString("000"));
                }
            }

            if (cancelDownload)
            {
                client.CancelAsync();
                client.Dispose();
            }
            else
            {
                allDownloaded = true;
            }
        }

        private void SpriteDownloader_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!allDownloaded && !cancelDownload)
                e.Cancel = true;
        }

        private void timerUpdateUI_Tick(object sender, EventArgs e)
        {
            try
            {
                progressBarFile.Maximum = pFileTotal;
                progressBarFile.Value = pFileCur;
                progressBarTotal.Maximum = pNumFiles;
                progressBarTotal.Value = pCurFile;
            }
            catch
            {
                // Lulzy way of preventing progress bar schenanigans when the current progress
                // is greater than the max progress thanks to the magic of threading
            }

            labelFile.Text = string.Format("File {0}/{1}", pCurFile, pNumFiles);
            labelFileSize.Text = string.Format("{0}/{1} bytes", pFileCur, pFileTotal);
            labelDownloading.Text = string.Format("Downloading {0}...", curFileName);
            labelStatus.Text = status;

            if (allDownloaded)
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are you sure you want to cancel downloading sprites?\n\nThe program cannot run until all sprites are downloaded.", "Really Exit?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
            {
                cancelDownload = true;
                if (Directory.Exists("Images"))
                {
                    foreach (string file in Directory.EnumerateFiles("Images"))
                    {
                        File.Delete(file);
                    }
                    Directory.Delete("Images");
                }
                this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            }
        }
    }
}
