﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace PokeViewer.Config
{
    public static class StringRepeat
    {
        // String.Repeat extension method
        public static string Repeat(this string str, int times)
        {
            string ret = "";
            for (int i = 0; i < times; i++)
            {
                ret += str;
            }
            return ret;
        }
    }

    public class ConfigFile
    {
        public delegate void BuildDefaultConfigDelegate(ConfigFile config);

        public string Name { get; private set; }
        public string Filename { get; private set; }
        public Node RootNode { get; private set; }
        public bool Loaded { get; private set; }

        public ConfigFile(string configFolder, string name, BuildDefaultConfigDelegate onBuildDefault)
        {
            this.Name = name;
            if (!Directory.Exists(configFolder))
                Directory.CreateDirectory(configFolder);
            this.RootNode = new Node("root");
            this.Filename = Path.Combine(configFolder, name + ".cfg");
            if (File.Exists(this.Filename))
            {
                if (ConfigLoader.ParseConfig(this))
                {
                    this.Loaded = true;
                }
                else
                {
                    this.Loaded = false;
                    this.RootNode = new Node("root");
                }
            }
            else
            {
                if (onBuildDefault != null)
                    onBuildDefault(this);
                SaveConfig();
            }
        }

        public void SaveConfig()
        {
            FileStream fs = new FileStream(this.Filename, FileMode.OpenOrCreate, FileAccess.ReadWrite);
            fs.SetLength(0);
            StreamWriter w = new StreamWriter(fs);

            foreach (Node node in this.RootNode.Children.Values)
                WriteNode(w, node, 0);

            foreach (string key in this.RootNode.Values.Keys)
            {
                w.WriteLine(string.Format("{0}={1}", key, this.RootNode.Values[key]));
            }

            w.Close();
            fs.Close();
        }

        void WriteNode(StreamWriter w, Node node, int indentation)
        {
            w.Write("\t".Repeat(indentation));
            w.WriteLine(node.Name);
            w.Write("\t".Repeat(indentation));
            w.WriteLine("{");
            foreach (Node child in node.Children.Values)
                WriteNode(w, child, indentation + 1);
            foreach (string key in node.Values.Keys)
            {
                w.Write("\t".Repeat(indentation + 1));
                w.WriteLine(string.Format("{0}={1}", key, node.Values[key]));
            }
            w.Write("\t".Repeat(indentation));
            w.WriteLine("}");
        }

        public bool GetValue<E>(string path, out E result)
        {
            string[] nodes = path.Split('.');
            result = default(E);
            if (nodes.Length <= 0)
                return false;
            string key = nodes[nodes.Length - 1];
            Node root = this.RootNode;
            for (int i = 0; i < nodes.Length - 1; i++)
            {
                if (!root.HasChild(nodes[i]))
                    return false;
                root = root.GetChild(nodes[i]);
            }
            if (root != null)
            {
                if (root.GetValue<E>(key, out result))
                    return true;
            }
            return false;
        }

    }
}
