﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace PokeViewer.Config
{
    static class ConfigLoader
    {

        public static bool ParseConfig(ConfigFile config)
        {
            FileStream fs = null;
            try
            {
                fs = new FileStream(config.Filename, FileMode.OpenOrCreate, FileAccess.ReadWrite);
                if (fs.CanRead)
                {
                    StreamReader r = new StreamReader(fs);
                    List<string> lines = new List<string>();
                    while (!r.EndOfStream)
                    {
                        lines.Add(r.ReadLine());
                    }
                    Node curRoot = config.RootNode;
                    for (int i = 0; i < lines.Count; i++)
                        if (lines[i].Trim() == "" || lines[i].Trim().StartsWith(";"))
                            lines.RemoveAt(i--);
                    for (int i = 0; i < lines.Count; i++)
                    {
                        string line = lines[i];
                        if (line.Contains('=')) // Key-value
                        {
                            string[] keyVal = line.Split(new char[] { '=' }, 2); // Value can contain '='
                            string key = keyVal[0].Trim();
                            string value = keyVal[1].Trim();
                            curRoot.SetValue(key, value);
                        }
                        else
                        {
                            // Beginning of group
                            if (line.Trim().EndsWith("{") || (i < lines.Count - 1 && lines[i + 1].Trim() == "{"))
                            {
                                if (i < lines.Count - 1 && lines[i + 1].Trim() == "{")
                                    i++;
                                string nName = line.Trim().Replace("{", "");
                                Node node = new Node(nName);
                                curRoot.AddNode(node);
                                curRoot = node;
                            }
                            // End of group
                            else if (line.Trim() == "}")
                            {
                                // Too many group closes
                                if (curRoot.Parent == null)
                                {
                                    Console.WriteLine("[{0}.cfg:{1}] Found \"}\" but there are no groups left to close", config.Name, i + 1);
                                    r.Close();
                                    fs.Close();
                                    return false;
                                }
                                curRoot = curRoot.Parent;
                            }
                            // Invalid line
                            else
                            {
                                Console.WriteLine("[{0}.cfg:{1}] Invalid line: \"{2}\"", config.Name, i + 1, line);
                                r.Close();
                                fs.Close();
                                return false;
                            }
                        }
                    }
                    // Not all groups were closed properly
                    if (curRoot != config.RootNode)
                    {
                        Console.WriteLine("[{0}.cfg:{1}] Unclosed group: {2}", config.Name, lines.Count, curRoot.Name);
                        r.Close();
                        fs.Close();
                        return false;
                    }

                    r.Close();
                    fs.Close();
                    return true;
                }
            }
            catch (IOException ex)
            {
                Console.WriteLine("Error loading config: ");
                Console.WriteLine(ex.ToString());
            }
            if (fs != null)
            {
                fs.Close();
            }
            return false;
        }

    }
}
