﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PokeViewer.Config
{
    public class Node
    {
        public string Name { get; private set; }
        public Node Parent { get; set; }

        public Dictionary<string, Node> Children { get; private set; }
        public Dictionary<string, string> Values { get; private set; }

        public Node(string name)
        {
            this.Name = name;
            this.Parent = null;
            this.Children = new Dictionary<string, Node>();
            this.Values = new Dictionary<string, string>();
        }

        public Node(string name, Node parent)
            : this(name)
        {
            this.Parent = parent;
        }

        public void AddNode(Node node)
        {
            node.Parent = this;
            this.Children.Add(node.Name, node);
        }

        public void SetValue(string key, object value)
        {
            if (!this.Values.ContainsKey(key))
                this.Values.Add(key, value.ToString());
            else
                this.Values[key] = value.ToString();
        }

        public bool HasChild(string name)
        {
            return this.Children.ContainsKey(name);
        }

        public Node GetChild(string name)
        {
            if (!HasChild(name))
                return null;
            return this.Children[name];
        }

        public bool HasKey(string key)
        {
            return this.Values.ContainsKey(key);
        }

        public bool GetValue<E>(string key, out E result)
        {
            if (this.Values.ContainsKey(key))
            {
                string val = this.Values[key];
                if (typeof(E) == typeof(string))
                {
                    result = (E)Convert.ChangeType(val, typeof(E));
                    return true;
                }
                else if (typeof(E) == typeof(int))
                {
                    int ret;
                    if (int.TryParse(val, out ret))
                    {
                        result = (E)Convert.ChangeType(ret, typeof(E));
                        return true;
                    }
                }
                else if (typeof(E) == typeof(double))
                {
                    double ret;
                    if (double.TryParse(val, out ret))
                    {
                        result = (E)Convert.ChangeType(ret, typeof(E));
                        return true;
                    }
                }
                else if (typeof(E) == typeof(float))
                {
                    float ret;
                    if (float.TryParse(val, out ret))
                    {
                        result = (E)Convert.ChangeType(ret, typeof(E));
                        return true;
                    }
                }
                else if (typeof(E) == typeof(bool))
                {
                    bool ret;
                    if (bool.TryParse(val, out ret))
                    {
                        result = (E)Convert.ChangeType(ret, typeof(E));
                        return true;
                    }
                }
            }
            result = default(E);
            return false;
        }

        public void PrintNode()
        {
            PrintNode(0);
        }

        private void PrintNode(int indentation)
        {
            Console.Write("\t".Repeat(indentation));
            Console.WriteLine(this.Name + ": ");
            foreach (Node child in Children.Values)
                child.PrintNode(indentation + 1);
            foreach (string key in Values.Keys)
            {
                Console.Write("\t".Repeat(indentation + 1));
                Console.WriteLine(string.Format("{0}: {1}", key, Values[key]));
            }
        }

    }
}
