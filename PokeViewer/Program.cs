﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PokeViewer
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            LogWriter logWriter = new LogWriter(Console.Out, "PokeViewer.log");
            Console.SetOut(logWriter);
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm());
        }

        private class LogWriter : TextWriter
        {
            private TextWriter _parent;
            private string _logFile;
            private FileStream _logStream;
            private string _buffer;

            public LogWriter(TextWriter parent, string logFile)
            {
                this._parent = parent;
                this._logFile = logFile;
                this._logStream = File.Open(this._logFile, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.Read);
                this._buffer = "";
            }

            public override void Write(char value)
            {
                this._parent.Write(value);
                this._buffer += value;
                if (value == '\n')
                {
                    if (this._logStream != null && this._logStream.CanWrite)
                    {
                        byte[] buf = Encoding.Default.GetBytes(this._buffer);
                        this._logStream.Write(buf, 0, buf.Length);
                        this._buffer = "";
                    }
                }
            }

            public override Encoding Encoding
            {
                get { return this._parent.Encoding; }
            }
        }
    }
}
