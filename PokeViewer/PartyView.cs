﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using PokeViewerLib;

namespace PokeViewer
{
    public partial class PartyView : UserControl
    {
        private int _lastSpecies = -1;
        private PartyMember _partyMember = null;
        private bool _compact = false;
        private Rectangle _hpRect;
        private Bitmap _empty;
        private Font _lvFontSmall, _lvFontLarge;

        public PartyMember PartyMember
        {
            get
            {
                return this._partyMember;
            }
            set
            {
                this._partyMember = value;
                Refresh();
            }
        }

        public bool Compact
        {
            get
            {
                return _compact;
            }
            set
            {
                this._compact = value;
                if (value)
                {
                    this.Height = 32;
                    spriteBox.Size = new Size(36, 32);
                    panelStats.Size = new Size(this.Width - spriteBox.Width, 32);
                    //panelHP.Visible = false;
                    this.panelHP.Location = new Point(6, 21);
                    this.panelHP.Size = new Size(118, 8);
                    this.labelStatus.Location = new Point(6, 17);
                    this.spriteBox.BackgroundImage = Properties.Resources.pokeball_small;
                }
                else
                {
                    this.Height = 72;
                    spriteBox.Size = new Size(72, 72);
                    panelStats.Size = new Size(this.Width - spriteBox.Width, 72);
                    //panelHP.Visible = true;
                    this.panelHP.Location = new Point(6, 33);
                    this.panelHP.Size = new Size(142, 10);
                    this.labelStatus.Location = new Point(6, 46);
                    this.spriteBox.BackgroundImage = Properties.Resources.pokeball_large;
                }
                this._lastSpecies = -1; // Force the sprites to update
                Refresh();
            }
        }

        public PartyView()
        {
            InitializeComponent();
            this.PartyMember = new PartyMember();
            this._hpRect = new Rectangle(0, 0, 0, 0);
            this._empty = new Bitmap(1, 1);
            this._lvFontSmall = new Font("Pokemon GB", 6f);
            this._lvFontLarge = new Font("Pokemon GB", 9f);
        }

        public override void Refresh()
        {
            base.Refresh();
            if (this._partyMember == null)
                return;
            if (this._partyMember.Data.Growth.Species == null)
            {
                this._lastSpecies = -1;
                this.labelNickname.Text = "";
                this.labelSpecies.Text = "";
                this.labelHP.Text = "";
                this.labelLevel.Text = "Lv. --";
                this.spriteBox.Image = this._empty;
                this.labelStatus.Visible = false;
                this.panelHP.Visible = false;
            }
            else
            {
                this.labelNickname.Text = this._partyMember.Nickname;
                this.labelSpecies.Text = string.Format("(#{0} {1})", this._partyMember.Data.Growth.Species.NationalDex.ToString("000"), this._partyMember.Data.Growth.Species.Name);
                if (this._compact)
                    this.labelHP.Text = string.Format("{0}/{1} HP", this._partyMember.CurrentHP.ToString().PadLeft(3), this._partyMember.MaximumHP, (int) ((float) this._partyMember.CurrentHP / (float) this._partyMember.MaximumHP * 100.0f));
                else
                    this.labelHP.Text = string.Format("{0}/{1} HP ({2}%)", this._partyMember.CurrentHP.ToString().PadLeft(3), this._partyMember.MaximumHP, (int) ((float) this._partyMember.CurrentHP / (float) this._partyMember.MaximumHP * 100.0f));
                this.labelLevel.Text = string.Format("Lv. {0}", this._partyMember.Level);
                this.labelStatus.Text = this._partyMember.GetStatusForDisplay().GetDisplayText();
                this.labelStatus.BackColor = this._partyMember.GetStatusForDisplay().GetColor();
                this.labelStatus.Visible = this._partyMember.GetStatusForDisplay() != DisplayStatus.None;
                if (this.Compact)
                {
                    if (this.labelStatus.Visible)
                    {
                        this.panelHP.Location = new Point(44, 21);
                        this.panelHP.Size = new Size(80, 8);
                    }
                    else
                    {
                        this.panelHP.Location = new Point(6, 21);
                        this.panelHP.Size = new Size(118, 8);
                    }
                }
                this.panelHP.Visible = true;
                this.labelNickname.ForeColor = (this.PartyMember.IsShiny() ? Color.Goldenrod : Color.White);

                Graphics g = this.spriteBox.CreateGraphics();
                StringFormat f = new StringFormat();
                f.Alignment = StringAlignment.Far;
                f.LineAlignment = StringAlignment.Far;
                g.DrawString(this.PartyMember.Level.ToString(), this._compact ? this._lvFontSmall : this._lvFontLarge, Brushes.White, this.spriteBox.Bounds, f);
                //g.DrawString("100", this._compact ? this._lvFontSmall : this._lvFontLarge, Brushes.White, this.spriteBox.Bounds, f);

                if (this._partyMember.Data.Growth.Species.NationalDex != _lastSpecies)
                {
                    this._lastSpecies = this._partyMember.Data.Growth.Species.NationalDex;
                    if (File.Exists(string.Format("Images/{0}_{1}.png", this.Compact ? "Icon" : "Sprite", this._lastSpecies.ToString("000"))))
                    {
                        this.spriteBox.Image = Image.FromFile(string.Format("Images/{0}_{1}.png", this.Compact ? "Icon" : "Sprite", this._lastSpecies.ToString("000") + (this.PartyMember.IsShiny() && !this.Compact ? "s" : "")));
                    }
                    else
                    {
                        this.spriteBox.Image = _empty;
                    }
                }
            }
        }

        private void panelHP_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            g.Clear(Color.Black);

            if (this._partyMember != null && this._partyMember.MaximumHP > 0)
            {
                int curHP = Math.Min(Math.Max((int) this._partyMember.CurrentHP, 0), this._partyMember.MaximumHP);
                int maxHP = this._partyMember.MaximumHP;
                float curPercent = (float) curHP / (float) maxHP;
                int fillWidth = (int) ((float) e.ClipRectangle.Width * curPercent);
                Brush fillBrush = (curPercent > 0.2 ? (curPercent > 0.5 ? Brushes.Lime : Brushes.Yellow) : Brushes.Red);
                this._hpRect.Width = fillWidth;
                this._hpRect.Height = e.ClipRectangle.Height;
                g.FillRectangle(fillBrush, this._hpRect);
                g.DrawRectangle(Pens.LightGray, 0, 0, this.panelHP.Width - 1, this.panelHP.Height - 1);
            }
        }

        private void ControlMouseDown(object sender, MouseEventArgs e)
        {
            OnMouseDown(e);
        }

        private void ControlDoubleClick(object sender, EventArgs e)
        {
            OnDoubleClick(e);
        }
    }

    public enum DisplayStatus
    {
        None,
        Fainted,
        Sleeping,
        Poisoned,
        Burned,
        Frozen,
        Paralyzed,
        BadlyPoisoned
    }

    public static class DisplayStatusExt
    {
        public static string GetDisplayText(this DisplayStatus status)
        {
            switch (status)
            {
                case DisplayStatus.Sleeping:
                    return "SLP";
                case DisplayStatus.Poisoned:
                case DisplayStatus.BadlyPoisoned:
                    return "PSN";
                case DisplayStatus.Burned:
                    return "BRN";
                case DisplayStatus.Frozen:
                    return "FRZ";
                case DisplayStatus.Paralyzed:
                    return "PAR";
                case DisplayStatus.Fainted:
                    return "FNT";
                default:
                    return "";
            }
        }

        public static Color GetColor(this DisplayStatus status)
        {
            switch (status)
            {
                case DisplayStatus.Sleeping:
                    return Color.Gray;
                case DisplayStatus.Poisoned:
                case DisplayStatus.BadlyPoisoned:
                    return Color.DarkViolet;
                case DisplayStatus.Burned:
                    return Color.DarkOrange;
                case DisplayStatus.Frozen:
                    return Color.Turquoise;
                case DisplayStatus.Paralyzed:
                    return Color.Gold;
                case DisplayStatus.Fainted:
                    return Color.Firebrick;
                default:
                    return Color.DarkGray;
            }
        }

        public static DisplayStatus GetStatusForDisplay(this PartyMember member)
        {
            if (member.CurrentHP == 0)
                return DisplayStatus.Fainted;
            else if (member.StatusCondition.SleepTurns > 0)
                return DisplayStatus.Sleeping;
            else if (member.StatusCondition.Poisoned)
                return DisplayStatus.Poisoned;
            else if (member.StatusCondition.Burned)
                return DisplayStatus.Burned;
            else if (member.StatusCondition.Frozen)
                return DisplayStatus.Frozen;
            else if (member.StatusCondition.Paralyzed)
                return DisplayStatus.Paralyzed;
            else if (member.StatusCondition.BadlyPoisoned)
                return DisplayStatus.BadlyPoisoned;
            return DisplayStatus.None;
        }
    }
}
