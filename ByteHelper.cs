﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PokeViewerLib
{
    /// <summary>
    /// Utility class for converting different data types between byte-array formats
    /// All conversions are done in little-endian
    /// </summary>
    public static class ByteHelper
    {
        /// <summary>
        /// Extends the IEnumerable class to allow substring-style accessing of elements
        /// </summary>
        /// <typeparam name="T">Type of data this IEnumerable represents</typeparam>
        /// <param name="array">Member-extension array. Left-hand-side of dot accessor.</param>
        /// <param name="start">Index of first element to pull</param>
        /// <param name="length">Number of elements to pull</param>
        /// <returns>Sub-array with designated bounds</returns>
        public static T[] Sub<T>(this IEnumerable<T> array, int start, int length)
        {
            return array.Skip(start).Take(length).ToArray<T>();
        }

        /// <summary>
        /// Converts a short (2-byte) integer into an array of bytes
        /// </summary>
        /// <param name="data">Short int</param>
        /// <returns>Byte array</returns>
        public static byte[] ShortToByteArray(ushort data)
        {
            return new byte[] { (byte) (data & 0xFF), (byte) ((data >> 8) & 0xFF) };
        }

        /// <summary>
        /// Converts a 4-byte integer into an array of bytes
        /// </summary>
        /// <param name="data">Integer</param>
        /// <returns>Byte array</returns>
        public static byte[] IntToByteArray(uint data)
        {
            return new byte[] { (byte)(data & 0xFF), (byte)((data >> 8) & 0xFF), (byte)((data >> 16) & 0xFF), (byte)((data >> 24) & 0xFF) };
        }

        /// <summary>
        /// Converts a string into an array of bytes
        /// </summary>
        /// <param name="data">String</param>
        /// <returns>Array of bytes</returns>
        public static byte[] StringToByteArray(string data)
        {
            byte[] buf = new byte[data.Length];
            for (int i = 0; i < data.Length; i++)
                buf[i] = (byte) data[i];
            return buf;
        }

        /// <summary>
        /// Converts an array of bytes into a short (2-byte) integer
        /// </summary>
        /// <param name="array">Byte array</param>
        /// <returns>Short int</returns>
        public static ushort ByteArrayToShort(byte[] array)
        {
            if (array.Length != 2)
                return 0;
            return (ushort) (array[0] +  (array[1] << 8));
        }

        /// <summary>
        /// Converts an array of bytes into a 4-byte integer
        /// </summary>
        /// <param name="array">Byte array</param>
        /// <returns>Integer</returns>
        public static uint ByteArrayToInt(byte[] array)
        {
            if (array.Length != 4)
                return 0;
            return (uint) (array[0] + (array[1] << 8) + (array[2] << 16) + (array[3] << 24));
        }

        /// <summary>
        /// Converts an array of bytes into a string
        /// </summary>
        /// <param name="array">Byte array</param>
        /// <returns>String</returns>
        public static string ByteArrayToString(byte[] array)
        {
            string temp = "";
            for (int i = 0; i < array.Length; i++)
                temp += (char)array[i];
            return temp;
        }
    }
}
