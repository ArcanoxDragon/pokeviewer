﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PokeViewerLib
{
    /// <summary>
    /// Represents all possible move types
    /// </summary>
    public enum MoveType
    {
        Normal,
        Fire,
        Water,
        Electric,
        Grass,
        Ice,
        Fighting,
        Poison,
        Ground,
        Flying,
        Psychic,
        Bug,
        Rock,
        Ghost,
        Dragon,
        Dark,
        Steel
    }

    /// <summary>
    /// Util class for dealing with move types.
    /// </summary>
    public static class TypeUtil
    {
        /// <summary>
        /// Returns the category for a specified move type using Generation 3 rules
        /// </summary>
        /// <param name="type">Move type</param>
        /// <returns>Move category</returns>
        public static Move.Category GetMoveCategoryByType(MoveType type)
        {
            switch (type)
            {
                case MoveType.Normal:
                    return Move.Category.Physical;
                case MoveType.Fire:
                    return Move.Category.Special;
                case MoveType.Water:
                    return Move.Category.Special;
                case MoveType.Electric:
                    return Move.Category.Special;
                case MoveType.Grass:
                    return Move.Category.Special;
                case MoveType.Ice:
                    return Move.Category.Special;
                case MoveType.Fighting:
                    return Move.Category.Physical;
                case MoveType.Poison:
                    return Move.Category.Physical;
                case MoveType.Ground:
                    return Move.Category.Physical;
                case MoveType.Flying:
                    return Move.Category.Physical;
                case MoveType.Psychic:
                    return Move.Category.Special;
                case MoveType.Bug:
                    return Move.Category.Physical;
                case MoveType.Rock:
                    return Move.Category.Physical;
                case MoveType.Ghost:
                    return Move.Category.Physical;
                case MoveType.Dragon:
                    return Move.Category.Special;
                case MoveType.Dark:
                    return Move.Category.Special;
                case MoveType.Steel:
                    return Move.Category.Physical;
            }

            return Move.Category.Status; // hur dur
        }
    }
}
