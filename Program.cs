﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using PokeViewerLib;
using System.IO;

namespace PokeViewer_Test
{
    /// <summary>
    /// This is the testing program. It will look for VisualBoyAdvance.exe and display
    /// a console window, displaying all current Party members in a text format.
    /// </summary>
    class Program
    {
        /// <summary>
        /// This is for loading a (slightly modified) version of the Move table from Bulbapedia
        /// It adds the moves from their format into the Move database and saves the database to
        /// moves.db. It should not need to be used unless the pre-shipped moves.db becomes corrupted.
        /// </summary>
        static void LoadTempMoves()
        {
            StreamReader r = new StreamReader("moves_tmp.txt");
            string line;

            while (!r.EndOfStream)
            {
                line = r.ReadLine();
                if (line.Trim().EndsWith("-")) // Start of move
                {
                    line = r.ReadLine().Trim().Substring(2); // Move index
                    int index;
                    if (int.TryParse(line, out index))
                    {
                        line = r.ReadLine().Trim().Substring(6); // Move name
                        line = line.Substring(0, line.Length - 2);
                        string name = line;
                        line = r.ReadLine().Trim(); // Move type
                        line = line.Substring(2, line.Length - 4);
                        line = line.Split('|')[1];
                        MoveType type;
                        if (!Enum.TryParse<MoveType>(line, out type))
                            type = MoveType.Normal;
                        line = r.ReadLine().Trim(); // Move category
                        line = line.Substring(2, line.Length - 4);
                        line = line.Split('|')[1];
                        Move.Category cat;
                        if (!Enum.TryParse<Move.Category>(line, out cat))
                            cat = Move.Category.Status;
                        line = r.ReadLine(); // Move contest type (ignore)
                        line = r.ReadLine().Trim().Substring(2); // Move PP
                        short maxPP;
                        if (short.TryParse(line, out maxPP))
                        {
                            try
                            {
                                line = r.ReadLine().Trim().Substring(2);
                            }
                            catch
                            {
                                line = "-1"; // So yeah, the move doesn't have a power
                            }
                            if (line == "_0_")
                                line = "-1"; // Durrrrrrrrrrrrrrr
                            short power;
                            if (short.TryParse(line, out power))
                            {
                                line = r.ReadLine().Trim().Substring(2); // Move accuracy
                                float accuracy = -1.0f;
                                bool cont = false;
                                if (line == "_0_")
                                {
                                    cont = true;
                                    if (cat != Move.Category.Status)
                                        accuracy = 2.0f;
                                }
                                else
                                {
                                    line = line.Substring(0, line.Length - 1);
                                    int t;
                                    if (cont = int.TryParse(line, out t)) // We *are* setting the variable, single equals is NOT a typo
                                    {
                                        accuracy = ((float)t) / 100.0f;
                                    }
                                }
                                if (cont)
                                {
                                    Move.AddMove((ushort) index, name, type, cat == Move.Category.Status, maxPP, power, accuracy);
                                }
                            }
                        }
                    }
                }
            }

            Move.SaveMoves(new FileStream("moves.db", FileMode.Create));
        }

        static void Main(string[] args)
        {
            Move.LoadMoves(new FileStream("moves.db", FileMode.Open));
            Species.LoadSpeciesList(new FileStream("species.db", FileMode.Open));

            ProcessWrapper wrap = ProcessWrapper.GetWrapperByProcessName("VisualBoyAdvance");

            if (wrap != null)
            {
                PokeDataReader reader = new PokeDataReader(wrap);
                PartyMember[] party = new PartyMember[6];

                bool running = true;
                while (running)
                {
                    Console.Clear();

                    for (int i = 0; i < 6; i++)
                    {
                        if (party[i] == null)
                            party[i] = new PartyMember();
                        reader.UpdatePartyMember(ref party[i], i);
                        Console.WriteLine("Party Member #{0}: {1}", i + 1, party[i]);
                    }

                    while (Console.KeyAvailable)
                        if (Console.ReadKey(true).Key == ConsoleKey.Escape)
                            running = false;

                    Thread.Sleep(250);
                }
            }
            else
            {
                Console.WriteLine("Could not find VisualBoyAdvance.");
            }

            Console.WriteLine("\nPress any key to continue");
            Console.ReadKey(true);
        }
    }
}
