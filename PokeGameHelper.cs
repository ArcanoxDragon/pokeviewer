﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PokeViewerLib
{
    /// <summary>
    /// Utility class that stores the known data offsets and types for Pokémon data
    /// </summary>
    public static class PokeGameHelper
    {
        /// <summary>
        /// Character code representing capital A
        /// </summary>
        const byte TEXT_UPPERCASE_A = 187;

        /// <summary>
        /// Character code representing capital Z
        /// </summary>
        const byte TEXT_UPPERCASE_Z = TEXT_UPPERCASE_A + 25;

        /// <summary>
        /// Character code representing lowercase A
        /// </summary>
        const byte TEXT_LOWERCASE_A = 213;

        /// <summary>
        /// Character code representing lowercase Z
        /// </summary>
        const byte TEXT_LOWERCASE_Z = TEXT_LOWERCASE_A + 25;

        /// <summary>
        /// The offset from the start of the game memory (not emulator memory)
        /// at which the Pokémon party data starts
        /// </summary>
        public static readonly Tuple<string, int>[] POKEDATA_OFFSETS = new Tuple<string, int>[] {
            new Tuple<string, int>("Ruby/Sapphire"   , 0x01004360),
            new Tuple<string, int>("Emerald (US)"    , 0x000244EC),
            new Tuple<string, int>("Emerald (Non-US)", 0x00024190),
            new Tuple<string, int>("FireRed"         , 0x00024284),
            new Tuple<string, int>("LeafGreen"       , 0x000241e4)
        };

        /// <summary>
        /// The size of one party member
        /// </summary>
        public const int SIZE_PARTY_MEMBER = 100;

        public const int OFF_PERSONALITY = 0;
        public const int OFF_OT_ID = 4;
        public const int OFF_NICKNAME = 8;
        public const int OFF_LANGUAGE = 18;
        public const int OFF_OT_NAME = 20;
        public const int OFF_MARKINGS = 27;
        public const int OFF_CHECKSUM = 28;
        public const int OFF_DATA = 32;
        public const int OFF_STATUS = 80;
        public const int OFF_LEVEL = 84;
        public const int OFF_POKERUS = 85;
        public const int OFF_CUR_HP = 86;
        public const int OFF_MAX_HP = 88;
        public const int OFF_ATTACK = 90;
        public const int OFF_DEFENSE = 92;
        public const int OFF_SPEED = 94;
        public const int OFF_SP_ATTACK = 96;
        public const int OFF_SP_DEFENSE = 98;

        public const int SIZE_PERSONALITY = 4;
        public const int SIZE_OT_ID = 4;
        public const int SIZE_NICKNAME = 10;
        public const int SIZE_LANGUAGE = 2;
        public const int SIZE_OT_NAME = 7;
        public const int SIZE_MARKINGS = 1;
        public const int SIZE_CHECKSUM = 2;
        public const int SIZE_DATA = 48;
        public const int SIZE_STATUS = 4;
        public const int SIZE_LEVEL = 1;
        public const int SIZE_POKERUS = 1;
        public const int SIZE_CUR_HP = 2;
        public const int SIZE_MAX_HP = 2;
        public const int SIZE_ATTACK = 2;
        public const int SIZE_DEFENSE = 2;
        public const int SIZE_SPEED = 2;
        public const int SIZE_SP_ATTACK = 2;
        public const int SIZE_SP_DEFENSE = 2;

        /// <summary>
        /// Converts an array of bytes representing in-game text to a C# string
        /// </summary>
        /// <param name="pokeText">Bytes representing in-game text</param>
        /// <returns>C# string converted from in-game text</returns>
        public static string PokeTextToString(byte[] pokeText)
        {
            string temp = "";
            foreach (byte b in pokeText)
            {
                if (b == 255)
                    return temp;
                if (b == 180)
                    temp += '\'';
                if (b >= TEXT_UPPERCASE_A && b <= TEXT_UPPERCASE_Z)
                {
                    temp += (char)('A' + (b - TEXT_UPPERCASE_A));
                }
                else if (b >= TEXT_LOWERCASE_A && b <= TEXT_LOWERCASE_Z)
                {
                    temp += (char)('a' + (b - TEXT_LOWERCASE_A));
                }
                /*else if (b != 0xFF && b != 0x00)
                    temp += " ";*/
            }
            return temp;
        }

        /// <summary>
        /// Converts a C# string to an array of bytes representing in-game text
        /// </summary>
        /// <param name="str">C# string to convert</param>
        /// <param name="maxLength">Maximum length of the text in-game</param>
        /// <returns>An array of bytes representing the text as it is stored in-game</returns>
        public static byte[] StringToPokeText(string str, int maxLength)
        {
            byte[] buf = new byte[maxLength];

            for (int i = 0; i < maxLength; i++)
            {
                if (i >= str.Length)
                    buf[i] = 0xFF;
                else
                {
                    char c = str[i];
                    if (c >= 'A' && c <= 'Z')
                    {
                        buf[i] = (byte)(TEXT_UPPERCASE_A + (byte)(c - 'A'));
                    }
                    else if (c >= 'a' && c <= 'z')
                    {
                        buf[i] = (byte)(TEXT_LOWERCASE_A + (byte)(c - 'a'));
                    }
                }
            }

            return buf;
        }
    }
}
