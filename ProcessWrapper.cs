﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Diagnostics;

namespace PokeViewerLib
{
    /// <summary>
    /// Wraps a Windows process allowing for easy memory reading/writing
    /// </summary>
    public class ProcessWrapper
    {
        /// <summary>
        /// Wraps raw kernel32.dll calls
        /// </summary>
        private static class MemoryHelper
        {
            public const int PROCESS_VM_READ = 0x0010;
            public const int PROCESS_VM_WRITE = 0x0020;
            public const int PROCESS_VM_OPERATION = 0x0008;

            [DllImport("kernel32.dll")]
            public static extern IntPtr OpenProcess(int dwDesiredAccess, bool bInheritHandle, int dwProcessId);

            [DllImport("kernel32.dll")]
            public static extern bool ReadProcessMemory(int hProcess, int lpBaseAddress, byte[] lpBuffer, int dwSize, ref int lpNumberOfBytesRead);

            [DllImport("kernel32.dll", SetLastError = true)]
            public static extern bool WriteProcessMemory(int hProcess, int lpBaseAddress, byte[] lpBuffer, int dwSize, ref int lpNumberOfBytesWritten);
        }

        private IntPtr _handle;
        private int _id;
        private string _name;

        private ProcessWrapper()
        {
            this._handle = default(IntPtr);
            this._id = -1;
            this._name = "";
        }

        /// <summary>
        /// Writes data to the process's memory
        /// </summary>
        /// <param name="offset">Memory offset to write</param>
        /// <param name="data">Data to write</param>
        /// <returns>True if write succeeded, false otherwise</returns>
        public bool WriteByte(int offset, byte data)
        {
            int written = 0;
            return MemoryHelper.WriteProcessMemory((int)this._handle, offset, new byte[] { data }, 1, ref written);
        }

        #region Writing
        /// <summary>
        /// Writes data to the process's memory
        /// </summary>
        /// <param name="offset">Memory offset to write</param>
        /// <param name="data">Data to write</param>
        /// <returns>True if write succeeded, false otherwise</returns>
        public bool WriteBytes(uint offset, byte[] data)
        {
            int written = 0;
            return MemoryHelper.WriteProcessMemory((int)this._handle, (int) offset, data, data.Length, ref written);
        }

        /// <summary>
        /// Writes a short to the process's memory
        /// </summary>
        /// <param name="offset">Memory offset to write</param>
        /// <param name="data">Data to write</param>
        /// <returns>True if write succeeded, false otherwise</returns>
        public bool WriteShort(uint offset, ushort data)
        {
            return WriteBytes(offset, ByteHelper.ShortToByteArray(data));
        }

        /// <summary>
        /// Writes an integer to the process's memory
        /// </summary>
        /// <param name="offset">Memory offset to write</param>
        /// <param name="data">Data to write</param>
        /// <returns>True if write succeeded, false otherwise</returns>
        public bool WriteInt(uint offset, uint data)
        {
            return WriteBytes(offset, ByteHelper.IntToByteArray(data));
        }

        /// <summary>
        /// Writes a string to the process's memory
        /// </summary>
        /// <param name="offset">Memory offset to write</param>
        /// <param name="data">Data to write</param>
        /// <returns>True if write succeeded, false otherwise</returns>
        public bool WriteString(uint offset, string data)
        {
            return WriteBytes(offset, ByteHelper.StringToByteArray(data));
        }
        #endregion

        #region Reading
        /// <summary>
        /// Reads a byte from the process's memory
        /// </summary>
        /// <param name="offset">Memory offset to read from</param>
        /// <param name="data">Variable in which to store read data</param>
        /// <returns>True if read succeeded, false otherwise</returns>
        public bool ReadByte(int offset, ref byte data)
        {
            int read = 0;
            byte[] buf = new byte[1];
            bool ret = MemoryHelper.ReadProcessMemory((int)this._handle, offset, buf, 1, ref read);
            if (ret)
                data = buf[0];
            return ret;
        }

        /// <summary>
        /// Reads an array of bytes from the process's memory
        /// </summary>
        /// <param name="offset">Memory offset to read from</param>
        /// <param name="data">Variable in which to store read data</param>
        /// <returns>True if read succeeded, false otherwise</returns>
        public bool ReadBytes(uint offset, ref byte[] data)
        {
            int read = 0;
            byte[] buf = new byte[data.Length];
            bool ret = MemoryHelper.ReadProcessMemory((int)this._handle, (int) offset, buf, data.Length, ref read);
            if (ret)
                buf.CopyTo(data, 0);
            return ret;
        }

        /// <summary>
        /// Reads a short from the process's memory
        /// </summary>
        /// <param name="offset">Memory offset to read from</param>
        /// <param name="data">Variable in which to store read data</param>
        /// <returns>True if read succeeded, false otherwise</returns>
        public bool ReadShort(uint offset, ref ushort data)
        {
            byte[] buf = new byte[2];
            if (ReadBytes(offset, ref buf))
            {
                data = ByteHelper.ByteArrayToShort(buf);
                return true;
            }
            return false;
        }

        /// <summary>
        /// Reads an integer from the process's memory
        /// </summary>
        /// <param name="offset">Memory offset to read from</param>
        /// <param name="data">Variable in which to store read data</param>
        /// <returns>True if read succeeded, false otherwise</returns>
        public bool ReadInt(uint offset, ref uint data)
        {
            byte[] buf = new byte[4];
            if (ReadBytes(offset, ref buf))
            {
                data = ByteHelper.ByteArrayToInt(buf);
                return true;
            }
            return false;
        }

        /// <summary>
        /// Reads a string from the process's memory
        /// </summary>
        /// <param name="offset">Memory offset to read from</param>
        /// <param name="data">Variable in which to store read data</param>
        /// <returns>True if read succeeded, false otherwise</returns>
        public bool ReadString(uint offset, ref string data, int length)
        {
            byte[] buf = new byte[length];
            if (ReadBytes(offset, ref buf))
            {
                data = ByteHelper.ByteArrayToString(buf);
                return true;
            }
            return false;
        }
        #endregion

        #region Static
        /// <summary>
        /// Gets a new ProcessWrapper wrapping the process with the specified name
        /// </summary>
        /// <remarks>
        /// If multiple processes are found matching the given name, the first one
        /// is chosen.
        /// </remarks>
        /// <param name="name">The name of the process (without ".exe", e.g. "notepad" instead of "notepad.exe")</param>
        /// <returns>A ProcessWrapper instance for the found process, or null if no process was found</returns>
        public static ProcessWrapper GetWrapperByProcessName(string name)
        {
            Process[] found = Process.GetProcessesByName(name);

            if (found.Length <= 0)
                return null;

            Process proc = found[0];
            ProcessWrapper wrapper = new ProcessWrapper();
            wrapper._id = proc.Id;
            wrapper._name = proc.ProcessName;
            wrapper._handle = MemoryHelper.OpenProcess(MemoryHelper.PROCESS_VM_READ | MemoryHelper.PROCESS_VM_WRITE | MemoryHelper.PROCESS_VM_OPERATION, false, wrapper._id);
            return wrapper;
        }
        #endregion
    }
}
