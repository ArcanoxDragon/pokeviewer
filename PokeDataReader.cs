﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PokeViewerLib
{
    /// <summary>
    /// Utility class to help read data from the emulator's memory
    /// </summary>
    public class PokeDataReader
    {
        /// <summary>
        /// This is the static (and thus hardcoded) location of the pointer that
        /// points to the game's memory inside the emulator's memory
        /// 
        /// Dereferences as:
        /// RAM_POINTER_LOCATION -> RAM Pointer -> Game memory
        /// </summary>
        private const int RAM_POINTER_LOCATION = 0x005A8F50;

        private uint _emuMemOffset = 0x00;
        private ProcessWrapper _wrap;
        private int _gameType;

        public int GameType
        {
            get
            {
                return this._gameType;
            }
            set
            {
                if (value >= 0 && value < PokeGameHelper.POKEDATA_OFFSETS.Length)
                    this._gameType = value;
            }
        }

        /// <summary>
        /// Constructs a new PokeDataReader
        /// </summary>
        /// <param name="emulatorProcess">The process wrapper representing the emulator</param>
        public PokeDataReader(ProcessWrapper emulatorProcess)
        {
            if (emulatorProcess != null)
            {
                this._wrap = emulatorProcess;
                this.GameType = 1; // Emerald (US)
                if (!LocateGameMemory())
                {
                    throw new UnauthorizedAccessException("Unable to read the emulator's memory!");
                }
            }
            else
            {
                throw new ArgumentNullException("emulatorProcess");
            }
        }

        /// <summary>
        /// Reads the value at RAM_POINTER_LOCATION, which in turn is the location
        /// of the game memory inside the emulator's memory
        /// </summary>
        private bool LocateGameMemory()
        {
            if (!this._wrap.ReadInt(RAM_POINTER_LOCATION, ref this._emuMemOffset))
            {
                this._emuMemOffset = 0x00;
                return false;
            }
            return true;
        }

        /// <summary>
        /// Updates the given party member with the data at the given party index in the game's memory
        /// </summary>
        /// <param name="member">The party member to update</param>
        /// <param name="partyIndex">The party index to pull data from (0-5)</param>
        public void UpdatePartyMember(ref PartyMember member, int partyIndex)
        {
            if (member == null)
                throw new ArgumentNullException("member");
            if (partyIndex < 0 || partyIndex > 5)
                throw new ArgumentOutOfRangeException("partyIndex", "Party index must be between 0 and 5");

            byte[] buf = new byte[PokeGameHelper.SIZE_PARTY_MEMBER];

            if (this._wrap.ReadBytes((uint) (this._emuMemOffset + PokeGameHelper.POKEDATA_OFFSETS[this._gameType].Item2 + (partyIndex * PokeGameHelper.SIZE_PARTY_MEMBER)), ref buf))
            {
                member.LoadFromDataArray(buf);
            }
        }
    }
}
