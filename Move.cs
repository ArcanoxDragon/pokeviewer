﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace PokeViewerLib
{

    /// <summary>
    /// Represents a move that can be learned by a Pokémon
    /// </summary>
    public class Move
    {
        /// <summary>
        /// Move category
        /// </summary>
        public enum Category
        {
            Physical,
            Special,
            Status
        }

        /// <summary>
        /// Singleton database of all loaded moves
        /// </summary>
        private static Dictionary<ushort, Move> _byIndex = new Dictionary<ushort, Move>();

        /// <summary>
        /// The index number of this move
        /// </summary>
        public ushort Index { get; private set; }

        /// <summary>
        /// The name of this move
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// The <seealso cref="Type"/> of this move
        /// </summary>
        public MoveType Type { get; private set; }

        /// <summary>
        /// The <seealso cref="Category"/> of this move
        /// </summary>
        public Category MoveCategory { get; private set; }

        /// <summary>
        /// The maximum number of PP (power-points) this move can have
        /// </summary>
        public short MaximumPP { get; private set; }

        /// <summary>
        /// The base power stat of this move (-1 means irrelevant)
        /// </summary>
        public short Power { get; private set; }

        /// <summary>
        /// The accuracy of this move
        /// 0.0 - 1.0 for normal accuracy, -1.0 for N/A, 2.0 for always-hit
        /// </summary>
        public float Accuracy { get; private set; }

        private Move(ushort index, string name, MoveType type, bool isStatus, short maxPP, short power, float accuracy)
        {
            this.Index = index;
            this.Name = name;
            this.Type = type;
            if (isStatus)
            {
                this.MoveCategory = Category.Status;
                this.Accuracy = -1.0f;
            }
            else
            {
                this.MoveCategory = TypeUtil.GetMoveCategoryByType(type);
                this.Accuracy = accuracy;
            }
            this.MaximumPP = maxPP;
            this.Power = power;
        }

        /// <summary>
        /// Returns the move with the specified index
        /// </summary>
        /// <param name="index">Move index</param>
        /// <returns>Move with given index</returns>
        public static Move GetMoveByIndex(ushort index)
        {
            if (_byIndex.ContainsKey(index))
                return _byIndex[index];
            return null;
        }

        /// <summary>
        /// Adds a move to the database
        /// </summary>
        /// <param name="index">Move index</param>
        /// <param name="name">Move name</param>
        /// <param name="type">Move type</param>
        /// <param name="isStatus">Whether or not move is a status move (as opposed to Physical or Special)</param>
        /// <param name="maxPP">Max amount of PP move can have</param>
        /// <param name="power">Base power of move</param>
        /// <param name="accuracy">Accuracy of move</param>
        public static void AddMove(ushort index, string name, MoveType type, bool isStatus, short maxPP, short power, float accuracy)
        {
            if (_byIndex.ContainsKey(index))
                throw new ArgumentException(string.Format("Move {0} already exists in the database!", index));
            _byIndex.Add(index, new Move(index, name, type, isStatus, maxPP, power, accuracy));
        }

        private static void ClearMoves()
        {
            _byIndex.Clear();
        }

        /// <summary>
        /// Reloads the move database from a file
        /// </summary>
        /// <param name="filename">Database filename</param>
        public static void LoadMoves(FileStream file)
        {
            ClearMoves();
            StreamReader r = new StreamReader(file);

            while (!r.EndOfStream)
            {
                string line = r.ReadLine();
                string[] split = line.Split('|');
                if (split.Length == 7)
                {
                    int index;
                    short maxPP, power;
                    float accuracy;
                    MoveType type;
                    Category cat;
                    if (int.TryParse(split[0], out index))
                    {
                        if (short.TryParse(split[4], out maxPP) && short.TryParse(split[5], out power))
                        {
                            if (float.TryParse(split[6], out accuracy))
                            {
                                if (Enum.TryParse<MoveType>(split[2], out type) && Enum.TryParse<Category>(split[3], out cat))
                                {
                                    AddMove((ushort) index, split[1], type, cat == Category.Status, maxPP, power, accuracy);
                                }
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Saves the move database to a file
        /// </summary>
        /// <param name="filename">Database filename</param>
        public static void SaveMoves(FileStream file)
        {
            StreamWriter w = new StreamWriter(file);

            foreach (Move m in _byIndex.Values)
            {
                w.WriteLine("{0}|{1}|{2}|{3}|{4}|{5}|{6}", m.Index, m.Name, m.Type.ToString(), m.MoveCategory.ToString(), m.MaximumPP, m.Power, m.Accuracy);
                w.Flush();
            }
        }
    }
}
