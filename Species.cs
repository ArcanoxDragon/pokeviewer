﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace PokeViewerLib
{
    /// <summary>
    /// Represents a Pokémon species
    /// </summary>
    public class Species
    {
        /// <summary>
        /// Singleton database of species
        /// </summary>
        private static Dictionary<int, Species> _byIndex = new Dictionary<int, Species>();

        /// <summary>
        /// Species index
        /// </summary>
        public int Index { get; private set; }

        /// <summary>
        /// National Pokédex number
        /// </summary>
        public int NationalDex { get; private set; }

        /// <summary>
        /// Species name
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// Species primary type
        /// </summary>
        public string Type1 { get; private set; }

        /// <summary>
        /// Species secondary type (can be an empty string if no secondary type)
        /// </summary>
        public string Type2 { get; private set; }

        private Species(int index, int nationalDex, string name, string type1, string type2)
        {
            this.Index = index;
            this.NationalDex = nationalDex;
            this.Name = name;
            this.Type1 = type1;
            if (type2 != type1)
                this.Type2 = type2;
            else
                this.Type2 = "";
        }

        /// <summary>
        /// Adds a species to the database
        /// </summary>
        /// <param name="index">The species index</param>
        /// <param name="nationalDex">The national Pokédex number of the species</param>
        /// <param name="name">The name of the species</param>
        /// <param name="type1">The primary type of the species</param>
        /// <param name="type2">The secondary type (if any) of the species</param>
        public static void AddSpecies(int index, int nationalDex, string name, string type1, string type2)
        {
            if (!_byIndex.ContainsKey(index))
            {
                _byIndex.Add(index, new Species(index, nationalDex, name, type1, type2));
            }
            else
            {
                throw new ArgumentException(string.Format("Species {0} already exists in the database!", index));
            }
        }

        /// <summary>
        /// Returns the species with the given index
        /// </summary>
        /// <param name="index">Index of species to find</param>
        /// <returns>Species matching given index or null if no species found</returns>
        public static Species GetSpeciesByIndex(int index)
        {
            if (_byIndex.ContainsKey(index))
                return _byIndex[index];
            return null;
        }

        /// <summary>
        /// Returns the species with the given national Pokédex number
        /// </summary>
        /// <param name="nationalDex">National Pokédex number of species</param>
        /// <returns>The species with the given national Pokédex number, or null if no species was found</returns>
        public static Species GetSpeciesByNationalDex(int nationalDex)
        {
            foreach (Species val in _byIndex.Values)
            {
                if (val.NationalDex == nationalDex)
                    return val;
            }
            return null;
        }

        private static void ClearSpeciesList()
        {
            _byIndex.Clear();
        }

        /// <summary>
        /// Reloads the species database from a file
        /// </summary>
        /// <param name="filename">Database filename</param>
        public static void LoadSpeciesList(FileStream file)
        {
            ClearSpeciesList();
            StreamReader r = new StreamReader(file);

            while (!r.EndOfStream)
            {
                string line = r.ReadLine();
                string[] split = line.Split('|');
                if (split.Length == 5)
                {
                    int index, nationalDex;
                    if (int.TryParse(split[0], out index) && int.TryParse(split[1], out nationalDex))
                    {
                        AddSpecies(index, nationalDex, split[2], split[3], split[4]);
                    }
                }
            }
        }

        /// <summary>
        /// Saves the species database to a file
        /// </summary>
        /// <param name="filename"></param>
        public static void SaveSpeciesList(FileStream file)
        {
            StreamWriter w = new StreamWriter(file);

            foreach (Species s in _byIndex.Values)
            {
                w.WriteLine("{0}|{1}|{2}|{3}|{4}", s.Index, s.NationalDex, s.Name, s.Type1, s.Type2);
            }

            w.Close();
        }
    }
}
