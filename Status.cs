﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PokeViewerLib
{
    public class StatusCondition
    {
        public int SleepTurns { get; set; }
        public bool Poisoned { get; set; }
        public bool BadlyPoisoned { get; set; }
        public bool Burned { get; set; }
        public bool Frozen { get; set; }
        public bool Paralyzed { get; set; }

        public StatusCondition(uint bitfield)
        {
            this.SleepTurns = ((int) bitfield & 0x7);
            this.Poisoned = ((int) bitfield & 0x8) > 0;
            this.Burned = ((int) bitfield & 0x10) > 0;
            this.Frozen = ((int) bitfield & 0x20) > 0;
            this.Paralyzed = ((int) bitfield & 0x40) > 0;
            this.BadlyPoisoned = ((int) bitfield & 0x80) > 0;
        }
    }
}
