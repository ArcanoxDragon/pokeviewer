PokeViewer is a utility that connects to VisualBoyAdvance and reads the RAM of a running Pokemon game, rendering the current party stats no matter where the player is in the game. For now it only works with Pokemon Emerald but will soon be expanded to cover all Gen III games and beyond that, Gen IV eventually.

To use it, either compile from source or download the binary from the Downloads page. Start VisualBoyAdvance and load up Pokemon Emerald, then start PokeViewer. It should find the emulator, but if not, click "Set Emulator Process" and type in the name of the emulator's EXE file, but without the .exe part.

I'd like to thank the folks over at Bulbapedia who compiled the massive collection of information on Pok�mon data structures. Without that data, this project would not have been possible.

This project's license is "don't steal it". Learn from the source but don't redistribute it (or the binary) or copy it or claim it as your own.